@extends('layouts.app')

@section('content')
    <div class="card">
        <h5 class="card-header">{!! trans('treatment.patient-treatment') !!} {{$patient->full_name}} [{{$patient->patientMRN}}]</h5>
        <div class="card-body">
            <div class="float-right mb-2">
                <button data-toggle="modal" data-target="#createTreatment" class="btn btn-sm btn-success hvr-glow">{!! trans('treatment.buttons.create-new') !!}</button>
            </div>
            <div class="table-responsive">
                <table class="table table-bordered table-sm" id="patientTreatmentList">
                    <thead class="text-center text-uppercase thead-dark">
                    <tr>
                        <th>Patient MRN</th>
                        <th>Full Name</th>
                        <th>Treatment Type</th>
                        <th>Date Created</th>
                        <th>Created By</th>
                        <th>Action</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($treatment AS $row)
                        <tr class="text-center">
                            <td class="align-middle">{{$patient->patientMRN}}</td>
                            <td class="align-middle">{{$patient->full_name}}</td>
                            <td class="align-middle">{{$row->treatmentType}}</td>
                            <td class="align-middle">{{$row->created_at}}</td>
                            <td class="align-middle">{{$row->created_by}}</td>
                            <td>
                                <a class="btn btn-sm btn-info btn-block hvr-glow"
                                   href="{{ route('treatment.create',$row->id) }}"
                                   data-toggle="tooltip"
                                   title="Treatment">
                                    {!! trans('patient.buttons.treatment-edit-patient') !!}
                                </a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>

    @include('modals.modal-create-treatment')
@endsection

@section('third_party_stylesheets')
    <link rel="stylesheet" href="{{asset('/plugins/DataTables/DataTables-1.10.25/css/dataTables.bootstrap4.min.css')}}"/>
@stop
@section('third_party_scripts')
    @include('scripts.datatables')
    <script type="text/javascript">
        $(function() {
            //Show n hide others textbox on selectbox selected
            $('#treatment_type').on('change', function() {
                $(this).val() === 'Lain-Lain' ? $('.isOthers').show() : $('.isOthers').hide();
            });
        });
    </script>
@stop



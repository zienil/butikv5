@extends('layouts.app')

@section('content')
    <div class="card">
        <h5 class="card-header">{!! trans('treatmentmanagement.create-new-treatment') !!}</h5>
        <div class="card-body">
            <input type="hidden" name="patientID" id="patientID" value="{{$patientid}}">
            <div class="form-group has-feedback row {{ $errors->has('treatment_type') ? ' has-error ' : '' }}">
                {!! Form::label('treatment_type', trans('forms.create_user_label_treatment_type'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                <div class="col-md-2">
                    @if($ptreatmentType === 'Baru')
                        <span class="badge badge-primary">{{$ptreatmentType}} @if($ptreatmentTypeDesc !== null)({{$ptreatmentTypeDesc}})@endif</span>
                    @elseif($ptreatmentType === 'Susulan')<span class="badge badge-success">{{$ptreatmentType}} @if($ptreatmentTypeDesc !== null)
                            ({{$ptreatmentTypeDesc}})@endif</span>
                    @elseif($ptreatmentType === 'Acute')<span class="badge badge-warning">{{$ptreatmentType}} @if($ptreatmentTypeDesc !== null)
                            ({{$ptreatmentTypeDesc}})@endif</span>
                    @elseif($ptreatmentType === 'Lain-Lain')<span class="badge badge-info">{{$ptreatmentType}} @if($ptreatmentTypeDesc !== null)
                            ({{$ptreatmentTypeDesc}})@endif</span>
                    @endif
                </div>
            </div>
            {{--START ACCORDION--}}
            <div class="accordion" id="accordionTreatment">
                {{--Current Diagnosis--}}
                <div class="card mb-0">
                    <div class="card-header" id="headingC1">
                        <h2 class="mb-0">
                            <button class="btn btn-link text-decoration-none" type="button"
                                    data-toggle="collapse"
                                    data-target="#c1" aria-expanded="true"
                                    aria-controls="c1">
                                {{trans('forms.collapse_label_current_diagnos')}}
                            </button>
                        </h2>
                    </div>

                    <div id="c1" class="collapse" aria-labelledby="headingC1" data-parent="#accordionTreatment">
                        {!! Form::open(array('route' => 'treatments.store', 'method' => 'POST', 'role' => 'form', 'class' => 'needs-validation')) !!}
                        {!! csrf_field() !!}
                        <input type="hidden" name="treatmentID" id="treatmentID" value="{{$ptreatmentid}}">
                        <input type="hidden" name="action" id="action" value="Diagnosis">
                        <div class="card-body">
                            <input type="hidden" name="action" id="action" value="Diagnosis">
                            <div class="form-group has-feedback row {{ $errors->has('current_diagnos') ? ' has-error ' : '' }}">
                                {!! Form::label('current_diagnos', trans('forms.create_user_label_current_diagnos'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                                <div class="col-md-6">
                                    {!! Form::textarea('current_diagnos', null, array('id' => 'current_diagnos', 'rows' => 4, 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_current_diagnos'))) !!}
                                    @if ($errors->has('current_diagnos'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('current_diagnos') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-10 offset-2">
                                    {!! Form::button(trans('forms.create_currdiag_button_text'), array('class' => 'btn btn-sm btn-success hvr-glow','type' => 'submit' )) !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                {{--Health History--}}
                <div class="card mb-0">
                    <div class="card-header" id="headingC2">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed text-decoration-none" type="button"
                                    data-toggle="collapse"
                                    data-target="#c2" aria-expanded="false"
                                    aria-controls="c2">
                                {{trans('forms.collapse_label_health_history')}}
                            </button>
                        </h2>
                    </div>
                    <div id="c2" class="collapse" aria-labelledby="headingC2"
                         data-parent="#accordionTreatment">
                        {!! Form::open(array('route' => 'treatments.store', 'method' => 'POST', 'role' => 'form', 'class' => 'needs-validation')) !!}
                        {!! csrf_field() !!}
                        <input type="hidden" name="treatmentID" id="treatmentID" value="{{$ptreatmentid}}">
                        <input type="hidden" name="action" id="action" value="HealthHistory">
                        <div class="card-body">
                            <div class="form-group has-feedback row {{ $errors->has('health_history') ? ' has-error ' : '' }}">
                                {!! Form::label('health_history', trans('forms.create_user_label_health_history'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                                <div class="col-md-6">
                                    @foreach($diseaseType AS $row1)
                                        <div class="row">
                                            <div class="col-md-6 col-xs-6">
                                                <div class="custom-control custom-switch">
                                                    <input class="custom-control-input diseaseCheckbox"
                                                           type="checkbox"
                                                           name="deseaseType[]"
                                                           id="deseaseType_{{ $row1->id }}"
                                                           data-for="infoBlast{{ $row1->id }}"
                                                           value="{{ $row1->descriptions }}">
                                                    <label class="custom-control-label"
                                                           for="deseaseType_{{ $row1->id }}">{{$row1->descriptions}}</label>
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-xs-6">
                                                @if(in_array($row1->id,[3,4,9,10,11,12]))
                                                    <div class="form-group row">
                                                        <div class="col-sm-10">
                                                            <input type="text"
                                                                   class="form-control form-control-sm infoBlast{{ $row1->id }}"
                                                                   name="infoBlast[{{ $row1->id }}]"
                                                                   id="infoBlast_{{ $row1->id }}" readonly>
                                                        </div>
                                                    </div>
                                                @endif
                                            </div>
                                        </div>

                                        @if ($errors->has('health_history'))
                                            <span class="help-block"><strong>{{ $errors->first('health_history') }}</strong></span>
                                        @endif
                                    @endforeach
                                </div>
                            </div>
                            <div class="form-group row">
                                <div class="col-sm-10 offset-2">
                                    {!! Form::button(trans('forms.create_hh_button_text'), array('class' => 'btn btn-sm btn-success hvr-glow','type' => 'submit' )) !!}
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
                {{--History of Treatment--}}
                <div class="card mb-0">
                    <div class="card-header" id="headingC3">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed text-decoration-none" type="button"
                                    data-toggle="collapse"
                                    data-target="#c3" aria-expanded="false"
                                    aria-controls="c3">
                                {{trans('forms.collapse_label_treatment_history')}}
                            </button>
                        </h2>
                    </div>
                    <div id="c3" class="collapse" aria-labelledby="headingC3"
                         data-parent="#accordionTreatment">
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover table-sm">
                                    <thead class="thead-dark">
                                    <tr>
                                        <th class="text-center">{{trans('forms.collapse_table_head_treatment_type')}}</th>
                                        <th class="text-center">{{trans('forms.collapse_table_head_treatment_type_specify')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($treatmentType AS $row2)
                                        <tr>
                                            <td>
                                                <div class="custom-control custom-switch">
                                                    <input class="custom-control-input treatmentCheckbox"
                                                           type="checkbox"
                                                           data-for="othersInfo{{ $row2->id }}"
                                                           name="treatmentType[]"
                                                           id="treatmentType_{{ $row2->id }}"
                                                           value="{{ $row2->descriptions }}">
                                                    <label class="custom-control-label"
                                                           for="treatmentType_{{ $row2->id }}">{{$row2->descriptions}}</label>
                                                </div>
                                            </td>
                                            <td>
                                                <input type="text"
                                                       class="form-control form-control-sm input-others othersInfo{{$row2->id}}"
                                                       name="othersInfo[]"
                                                       id="othersInfo" readonly>
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                {{--Vital Signs--}}
                <div class="card mb-0">
                    <div class="card-header" id="headingC4">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed text-decoration-none" type="button"
                                    data-toggle="collapse"
                                    data-target="#c4" aria-expanded="false"
                                    aria-controls="c4">
                                {{trans('forms.collapse_label_vital_point')}}
                            </button>
                        </h2>
                    </div>
                    <div id="c4" class="collapse" aria-labelledby="headingC4"
                         data-parent="#accordionTreatment">
                        <div class="card-body">
                            <div class="form-row">
                                <div class="form-group col-md-3 has-feedback {{ $errors->has('blood_pressure') ? ' has-error ' : '' }}">
                                    {!! Form::label('blood_pressure', trans('forms.create_user_label_blood_pressure'), array('class' => 'control-label col-form-label-sm font-weight-bold')); !!}
                                    {!! Form::text('blood_pressure', null, array('id' => 'blood_pressure', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_blood_pressure'))) !!}
                                    @if ($errors->has('blood_pressure'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('blood_pressure') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-3 has-feedback {{ $errors->has('weight') ? ' has-error ' : '' }}">
                                    {!! Form::label('weight', trans('forms.create_user_label_weight'), array('class' => 'control-label col-form-label-sm font-weight-bold')); !!}
                                    {!! Form::text('weight', null, array('id' => 'weight', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_weight'))) !!}
                                    @if ($errors->has('weight'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('weight') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-3 has-feedback {{ $errors->has('high') ? ' has-error ' : '' }}">
                                    {!! Form::label('high', trans('forms.create_user_label_high'), array('class' => 'control-label col-form-label-sm font-weight-bold')); !!}
                                    {!! Form::text('high', null, array('id' => 'high', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_high'))) !!}
                                    @if ($errors->has('high'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('high') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-3 has-feedback {{ $errors->has('pulse') ? ' has-error ' : '' }}">
                                    {!! Form::label('pulse', trans('forms.create_user_label_pulse'), array('class' => 'control-label col-form-label-sm font-weight-bold')); !!}
                                    {!! Form::text('pulse', null, array('id' => 'pulse', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_pulse'))) !!}
                                    @if ($errors->has('pulse'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('pulse') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-3 has-feedback {{ $errors->has('body_temperature') ? ' has-error ' : '' }}">
                                    {!! Form::label('body_temperature', trans('forms.create_user_label_body_temperature'), array('class' => 'control-label col-form-label-sm font-weight-bold')); !!}
                                    {!! Form::text('body_temperature', null, array('id' => 'pulse', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_body_temperature'))) !!}
                                    @if ($errors->has('body_temperature'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('body_temperature') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-3 has-feedback {{ $errors->has('glucose') ? ' has-error ' : '' }}">
                                    {!! Form::label('glucose', trans('forms.create_user_label_glucose'), array('class' => 'control-label col-form-label-sm font-weight-bold')); !!}
                                    {!! Form::text('glucose', null, array('id' => 'glucose', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_glucose'))) !!}
                                    @if ($errors->has('glucose'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('glucose') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                                <div class="form-group col-md-6 has-feedback {{ $errors->has('pain') ? ' has-error ' : '' }}">
                                    {!! Form::label('pain', trans('forms.create_user_label_pain'), array('class' => 'control-label col-form-label-sm font-weight-bold')); !!}
                                    @for($i=1;$i<=10;$i++)
                                        <div class="custom-control custom-radio custom-switch">
                                            <input type="radio" id="pain_{{$i}}"
                                                   name="painRadio" class="custom-control-input">
                                            <label class="custom-control-label"
                                                   for="pain_{{$i}}">{{$i}}</label>
                                        </div>
                                    @endfor
                                    @if ($errors->has('pain'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('pain') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--Analysis Justification--}}
                <div class="card mb-0">
                    <div class="card-header" id="headingC5">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed text-decoration-none" type="button"
                                    data-toggle="collapse"
                                    data-target="#c5" aria-expanded="false"
                                    aria-controls="c5">
                                {{trans('forms.collapse_label_analysis_justification')}}
                            </button>
                        </h2>
                    </div>
                    <div id="c5" class="collapse" aria-labelledby="headingC5"
                         data-parent="#accordionTreatment">
                        <div class="card-body">
                            <div class="form-group has-feedback row {{ $errors->has('das') ? ' has-error ' : '' }}">
                                {!! Form::label('das', trans('forms.create_user_label_das'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                                <div class="col-md-6">
                                    {!! Form::text('das', null, array('id' => 'das','class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_das'))) !!}
                                    @if ($errors->has('das'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('das') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('tls1') ? ' has-error ' : '' }}">
                                {!! Form::label('tls1', trans('forms.create_user_label_tls1'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                                <div class="col-md-6">
                                    {!! Form::text('tls1', null, array('id' => 'tls1','class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_tls1'))) !!}
                                    @if ($errors->has('tls1'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('tls1') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('tls2') ? ' has-error ' : '' }}">
                                {!! Form::label('tls2', trans('forms.create_user_label_tls2'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                                <div class="col-md-6">
                                    {!! Form::text('tls2', null, array('id' => 'tls2','class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_tls3'))) !!}
                                    @if ($errors->has('tls2'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('tls2') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('tls3') ? ' has-error ' : '' }}">
                                {!! Form::label('tls3', trans('forms.create_user_label_tls3'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                                <div class="col-md-6">
                                    {!! Form::text('tls3', null, array('id' => 'tls3','class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_tls3'))) !!}
                                    @if ($errors->has('tls3'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('tls3') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('ps') ? ' has-error ' : '' }}">
                                {!! Form::label('ps', trans('forms.create_user_label_ps'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                                <div class="col-md-6">
                                    {!! Form::text('ps', null, array('id' => 'ps','class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_ps'))) !!}
                                    @if ($errors->has('ps'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('ps') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('analysis_notes') ? ' has-error ' : '' }}">
                                {!! Form::label('analysis_notes', trans('forms.create_user_label_analysis_notes'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                                <div class="col-md-6">
                                    {!! Form::textarea('analysis_notes', null, array('id' => 'analysis_notes', 'rows' => 4, 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_analysis_notes'))) !!}
                                    @if ($errors->has('analysis_notes'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('analysis_notes') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                            <hr/>
                            <div class="form-group has-feedback row {{ $errors->has('tkms') ? ' has-error ' : '' }}">
                                {!! Form::label('tkms', trans('forms.create_user_label_tkms'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                                <div class="col-md-6">
                                    {!! Form::text('tkms', null, array('id' => 'tkms','class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_tkms'))) !!}
                                    @if ($errors->has('tkms'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('tkms') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('pkm') ? ' has-error ' : '' }}">
                                {!! Form::label('pkm', trans('forms.create_user_label_pkm'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                                <div class="col-md-6">
                                    {!! Form::text('pkm', null, array('id' => 'pkm','class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_pkm'))) !!}
                                    @if ($errors->has('pkm'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('pkm') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('justification') ? ' has-error ' : '' }}">
                                {!! Form::label('justification', trans('forms.create_user_label_justification'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                                <div class="col-md-6">
                                    {!! Form::textarea('justification', null, array('id' => 'justification', 'rows' => 4, 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_justification'))) !!}
                                    @if ($errors->has('justification'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('justification') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('contra_indication') ? ' has-error ' : '' }}">
                                {!! Form::label('contra_indication', trans('forms.create_user_label_contra_indication'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                                <div class="col-md-6">
                                    {!! Form::textarea('contra_indication', null, array('id' => 'contra_indication', 'rows' => 4, 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_contra_indication'))) !!}
                                    @if ($errors->has('contra_indication'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('contra_indication') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('tap') ? ' has-error ' : '' }}">
                                {!! Form::label('tap', trans('forms.create_user_label_tap'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                                <div class="col-md-6">
                                    {!! Form::textarea('tap', null, array('id' => 'tap', 'rows' => 4, 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_tap'))) !!}
                                    @if ($errors->has('tap'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('tap') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--Observation of Treatment--}}
                <div class="card mb-0">
                    <div class="card-header" id="headingC6">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed text-decoration-none" type="button"
                                    data-toggle="collapse"
                                    data-target="#c6" aria-expanded="false"
                                    aria-controls="c6">
                                {{trans('forms.collapse_label_observation_treatment')}}
                            </button>
                        </h2>
                    </div>
                    <div id="c6" class="collapse" aria-labelledby="headingC6"
                         data-parent="#accordionTreatment">
                        <div class="card-body">
                            <div class="form-group has-feedback row {{ $errors->has('therapy_type') ? ' has-error ' : '' }}">
                                {!! Form::label('therapy_type', trans('forms.create_user_label_therapy_type'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                                <div class="col-md-2">
                                    <select class="form-control form-control-sm" name="therapy_type"
                                            id="therapy_type">
                                        <option>{{ trans('forms.create_user_ph_therapy_type') }}</option>
                                        <option value="Terapi Mandian">Terapi Mandian</option>
                                        <option value="Terapi Kimiawi">Terapi Kimiawi</option>
                                        <option value="Terapi Urutan">Terapi Urutan</option>
                                        <option value="Terapi Bekam">Terapi Bekam</option>
                                    </select>
                                    @if ($errors->has('therapy_type'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('therapy_type') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('odt') ? ' has-error ' : '' }}">
                                {!! Form::label('odt', trans('forms.create_user_label_odt'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                                <div class="col-md-6">
                                    {!! Form::textarea('odt', null, array('id' => 'odt', 'rows' => 4, 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_odt'))) !!}
                                    @if ($errors->has('odt'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('odt') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('rtop') ? ' has-error ' : '' }}">
                                {!! Form::label('rtop', trans('forms.create_user_label_rtop'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                                <div class="col-md-6">
                                    {!! Form::textarea('rtop', null, array('id' => 'rtop', 'rows' => 4, 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_rtop'))) !!}
                                    @if ($errors->has('rtop'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('rtop') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('ftp') ? ' has-error ' : '' }}">
                                {!! Form::label('ftp', trans('forms.create_user_label_ftp'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                                <div class="col-md-6">
                                    {!! Form::textarea('ftp', null, array('id' => 'ftp', 'rows' => 4, 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_ftp'))) !!}
                                    @if ($errors->has('ftp'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('ftp') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--Follow up treatment--}}
                <div class="card mb-0">
                    <div class="card-header" id="headingC7">
                        <h2 class="mb-0">
                            <button class="btn btn-link collapsed text-decoration-none" type="button"
                                    data-toggle="collapse"
                                    data-target="#c7" aria-expanded="false"
                                    aria-controls="c7">
                                {{trans('forms.collapse_label_follow_up_treatment')}}
                            </button>
                        </h2>
                    </div>
                    <div id="c7" class="collapse" aria-labelledby="headingC7"
                         data-parent="#accordionTreatment">
                        <div class="card-body">
                            <div class="form-group has-feedback row {{ $errors->has('n3d') ? ' has-error ' : '' }}">
                                {!! Form::label('n3d', trans('forms.create_user_label_n3d'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                                <div class="col-md-6">
                                    {!! Form::textarea('n3d', null, array('id' => 'n3d', 'rows' => 4, 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_n3d'))) !!}
                                    @if ($errors->has('n3d'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('n3d') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('n7d') ? ' has-error ' : '' }}">
                                {!! Form::label('n7d', trans('forms.create_user_label_n7d'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                                <div class="col-md-6">
                                    {!! Form::textarea('n7d', null, array('id' => 'n7d', 'rows' => 4, 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_n7d'))) !!}
                                    @if ($errors->has('n7d'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('n7d') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('n14d') ? ' has-error ' : '' }}">
                                {!! Form::label('n14d', trans('forms.create_user_label_n14d'), array('class' => 'col-md-2 control-label col-form-label-sm font-weight-bold')); !!}
                                <div class="col-md-6">
                                    {!! Form::textarea('n14d', null, array('id' => 'n14d', 'rows' => 4, 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_n14d'))) !!}
                                    @if ($errors->has('n14d'))
                                        <span class="help-block">
                                                        <strong>{{ $errors->first('n14d') }}</strong>
                                                    </span>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('third_party_stylesheets')

@stop
@section('third_party_scripts')
    <script type="text/javascript">
        $(function() {
            //Show n hide others textbox on selectbox selected
            $('#treatment_type').on('change', function() {
                $(this).val() == 'Lain-Lain' ? $('.isOthers').show() : $('.isOthers').hide();
            });

            //CHECKBOX
            // if checked, enable textbox
            $(document).on('change', '.diseaseCheckbox', function() {
                var $this = $(this);
                var $target = $('input.' + $this.attr('data-for'));
                if ($this.is(':checked')) {
                    $target.prop('readonly', false).css('background-color', 'white');
                } else {
                    $target.prop('readonly', true).css('background-color', '#ededed');
                }
            });

            $(document).on('change', '.treatmentCheckbox', function() {
                var $this = $(this);
                var $target = $('input.' + $this.attr('data-for'));
                $target.prop('readonly', !$(this).is(':checked'));
            });
            $('.input-others').each(function(e) {
                if ($(this).val() === '') {
                    $(this).prop('readonly', true);
                } else {
                    $(this).prop('readonly', false);
                }
            });
        });


    </script>
@stop



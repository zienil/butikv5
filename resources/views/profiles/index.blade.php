@extends('layouts.app')

@section('content')
    <div class="card">
        <h5 class="card-header">
            <i class='fas fa-user mr-2'></i> {{ $user->username }}'s {{trans('profile.userProfile')}}
        </h5>
        <div class="card-body">
            <div class="row">
                <div class="col-2 text-center">
                    @if ($user->profile)
                        @if(!empty($user->profile->avatar))
                            <img
                                src="@if ($user->profile->avatar_status == 1) {{ $user->profile->avatar }} @endif"
                                alt="{{ $user->username }}"
                                class="user-avatar rounded-circle border border-dark">
                        @else
                            <i class="fas fa-user-circle fa-9x"></i>
                        @endif
                    @else
                        <i class="fas fa-user-circle fa-9x"></i>
                    @endif
                </div>
                <div class="col-5">
                    <dl>
                        <dt class="col-sm-4">{{trans('profile.showProfileFullName')}}</dt>
                        <dd class="col-sm-8">{{ $user->fullname }}</dd>
                        <dt class="col-sm-4">{{trans('profile.showProfilemykadProfile')}}</dt>
                        <dd class="col-sm-8">
                            @if(!empty($user->profile()))
                                @if($user->profile->icnumber == null || $user->profile->icnumber == '')
                                    <span class="badge badge-warning">NULL</span>
                                @else
                                    {{ $user->profile->icnumber }}
                                @endif
                            @endif
                        </dd>
                        <dt class="col-sm-4">{{trans('profile.showProfileUsername')}}</dt>
                        <dd class="col-sm-8">{{ $user->username }}</dd>
                        <dt class="col-sm-4">{{trans('profile.showProfileEmail')}}</dt>
                        <dd class="col-sm-8">{{ $user->email }}</dd>
                        <dt class="col-sm-4">{{trans('profile.showProfilePhone')}}</dt>
                        <dd class="col-sm-8">
                            @if($user->profile->phone == null || $user->profile->phone == '')
                                <span class="badge badge-warning">NULL</span>
                            @else
                                {{ $user->profile->phone }}
                            @endif
                        </dd>
                        <dt class="col-sm-4">{{trans('profile.showProfilePrivilege')}}</dt>
                        <dd class="col-sm-8">
                            @if(!empty($user->getRoleNames()))
                                @foreach($user->getRoleNames() as $v)
                                    <label class="badge badge-info">{{ $v }}</label>
                                @endforeach
                            @endif
                        </dd>
                    </dl>
                </div>

                <div class="col-5">
                    <dl>
                        <dt class="col-sm-4">{{trans('profile.showAddressProfile')}}</dt>
                        <dd class="col-sm-8">
                            {{$user->profile->address}},
                            {{$user->profile->postcode}},
                            {{$user->profile->state}},
                            {{$user->profile->country}}
                        </dd>
                        <dt class="col-sm-4">{{trans('profile.showAgeProfile')}}</dt>
                        <dd class="col-sm-8">
                            @if($user->profile->age == null || $user->profile->age == '')
                                <span class="badge badge-warning">NULL</span>
                            @else
                                {{ $user->profile->age }}
                            @endif
                        </dd>
                        <dt class="col-sm-4">{{trans('profile.showGenderProfile')}}</dt>
                        <dd class="col-sm-8">
                            @if($user->profile->gender == null || $user->profile->gender == '')
                                <span class="badge badge-warning">NULL</span>
                            @else
                                {{ $user->profile->gender }}
                            @endif
                        </dd>
                        <dt class="col-sm-4">{{trans('profile.showRaceProfile')}}</dt>
                        <dd class="col-sm-8">
                            @if($user->profile->race == null || $user->profile->race == '')
                                <span class="badge badge-warning">NULL</span>
                            @else
                                {{ $user->profile->race }}
                            @endif
                        </dd>
                        <dt class="col-sm-4">{{trans('profile.showOccupationProfile')}}</dt>
                        <dd class="col-sm-8">
                            @if($user->profile->occupation == null || $user->profile->occupation == '')
                                <span class="badge badge-warning">NULL</span>
                            @else
                                {{ $user->profile->occupation }}
                            @endif
                        </dd>
                        <dt class="col-sm-4">{{trans('profile.showProfileButiks')}}'s</dt>
                        <dd class="col-sm-8">
                        </dd>
                    </dl>
                </div>
            </div>
            @if ($user->profile)
                @if (Auth::user()->id == $user->id)
                    <a class="btn btn-success btn-block" href="{{ route('profile.edit',Auth::user()->username) }}">{!! trans('buttons.buttons.edit-profile') !!}</a>
                @endif
            @endif
        </div>
    </div>
@endsection

@section('third_party_scripts')
@stop



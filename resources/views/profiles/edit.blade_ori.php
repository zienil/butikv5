@extends('layouts.app')

@section('content')
    <div class="card">
        <h5 class="card-header">
            <i class='fas fa-user mr-2'></i>{{ trans('profile.templateTitle') }}
            <div class="float-right">
                <a class="btn btn-outline-warning btn-sm"
                   href="{{ route('profile.index') }}"> {!!trans('forms.buttons.back')!!}</a>
            </div>
        </h5>
        <div class="card-body">
            @if ($user->profile)
                @if (Auth::user()->id == $user->id)
                    {{--            <div class="row mb-1">--}}
                    {{--                <div class="col-sm-12">--}}
                    {{--                    @if ($user->profile)--}}
                    {{--                        <div id="avatar_container">--}}
                    {{--                            <div--}}
                    {{--                                class="collapseOne card-collapse collapse @if($user->profile->avatar_status == 0) show @endif">--}}
                    {{--                                <div class="card-body">--}}
                    {{--                                    <img src="#" alt="{{ $user->username }}" class="user-avatar">--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                            <div--}}
                    {{--                                class="collapseTwo card-collapse collapse @if($user->profile->avatar_status == 1) show @endif">--}}
                    {{--                                <div class="card-body">--}}
                    {{--                                    <div class="dz-preview"></div>--}}
                    {{--                                    {!! Form::open(array('route' => 'avatar.upload', 'method' => 'POST', 'name' => 'avatarDropzone','id' => 'avatarDropzone', 'class' => 'form single-dropzone dropzone single', 'files' => true)) !!}--}}
                    {{--                                    <img id="user_selected_avatar" class="user-avatar"--}}
                    {{--                                         src="@if ($user->profile->avatar != null) {{ $user->profile->avatar }} @endif"--}}
                    {{--                                         alt="{{ $user->username }}">--}}
                    {{--                                    {!! Form::close() !!}--}}
                    {{--                                </div>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    @else--}}
                    {{--                        <div class="card bg-light">--}}
                    {{--                            <div class="card-body">--}}
                    {{--                                <h5 class="card-title">Profile Picture Not Exist</h5>--}}
                    {{--                                <p class="card-text">You don't have any profile picture in Purnama database.</p>--}}
                    {{--                                <a href="#" class="btn btn-sm btn-primary"--}}
                    {{--                                   onclick="event.preventDefault(); document.getElementById('activate-avatar-form').submit();">Click--}}
                    {{--                                    Here to Update</a>--}}
                    {{--                                <form id="activate-avatar-form" action="{{ route('profile.activate') }}"--}}
                    {{--                                      method="GET" class="d-none">--}}
                    {{--                                    @csrf--}}
                    {{--                                </form>--}}
                    {{--                            </div>--}}
                    {{--                        </div>--}}
                    {{--                    @endif--}}
                    {{--                </div>--}}
                    {{--            </div>--}}
                    <div class="card border-primary mt-3">
                        <div class="card-body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-pills nav-justified" id="myTab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link active" href=".edit-profile-tab"
                                       data-toggle="pill" role="tab" aria-controls="edit-profile-tab"
                                       aria-selected="true">{{trans('profile.editProfileTitle')}}</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href=".edit-settings-tab" data-toggle="pill"
                                       role="tab" aria-controls="edit-settings-tab"
                                       aria-selected="false">{{trans('profile.editAccountTitle')}}</a>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <a class="nav-link" href=".edit-account-tab" data-toggle="pill"
                                       role="tab" aria-controls="edit-settings-tab"
                                       aria-selected="false">{{trans('profile.editAccountAdminTitle')}}</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content mt-1" id="v-pills-tabContent">
                                <div class="tab-pane fade show active edit-profile-tab" role="tabpanel"
                                     aria-labelledby="edit-profile-tab">
                                    <div class="row mb-1">
                                        <div class="col-sm-12">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::model($user, ['method' => 'PATCH','route' => ['profile.update']]) !!}
                    <div class="card border-primary mt-3">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">{{trans('forms.username')}}</label>
                                        <div class="col-sm-8">
                                            {!! Form::text('username', null, array('class' => 'form-control-plaintext','readonly')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">{{trans('forms.fullname')}}</label>
                                        <div class="col-sm-8">
                                            {!! Form::text('fullname', null, array('placeholder' => 'Full Name','class' => 'form-control form-control-sm')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">{{trans('forms.mykadProfile')}}</label>
                                        <div class="col-sm-8">
                                            {!! Form::text('icnumber', null, array('placeholder' => 'MyKAD/Passport','class' => 'form-control form-control-sm')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">{{trans('forms.ageProfile')}}</label>
                                        <div class="col-sm-8">
                                            {!! Form::text('age', null, array('placeholder' => 'Age','class' => 'form-control form-control-sm')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">{{trans('forms.email')}}</label>
                                        <div class="col-sm-8">
                                            {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control form-control-sm')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">{{trans('forms.password')}}</label>
                                        <div class="col-sm-8">
                                            {!! Form::password('password', array('placeholder' => 'Password','class' => 'form-control form-control-sm')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">{{trans('forms.confirm_pass')}}</label>
                                        <div class="col-sm-8">
                                            {!! Form::password('confirm-password', array('placeholder' => 'Confirm Password','class' => 'form-control form-control-sm')) !!}
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">{{trans('forms.genderProfile')}}</label>
                                        <div class="col-sm-8">
                                            {!! Form::text('gender', null, array('placeholder' => 'Gender','class' => 'form-control form-control-sm')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">{{trans('forms.raceProfile')}}</label>
                                        <div class="col-sm-8">
                                            {!! Form::text('race', null, array('placeholder' => 'Race','class' => 'form-control form-control-sm')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">{{trans('forms.addressProfile')}}</label>
                                        <div class="col-sm-8">
                                            {!! Form::text('address', null, array('placeholder' => 'Full Address','class' => 'form-control form-control-sm')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label
                                            class="col-sm-4 col-form-label">{{trans('forms.postcodeProfile')}}</label>
                                        <div class="col-sm-8">
                                            {!! Form::text('postcode', null, array('placeholder' => 'Postcode','class' => 'form-control form-control-sm')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">{{trans('forms.stateProfile')}}</label>
                                        <div class="col-sm-8">
                                            {!! Form::text('state', null, array('placeholder' => 'State','class' => 'form-control form-control-sm')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label class="col-sm-4 col-form-label">{{trans('forms.countryProfile')}}</label>
                                        <div class="col-sm-8">
                                            {!! Form::text('country', null, array('placeholder' => 'Country','class' => 'form-control form-control-sm')) !!}
                                        </div>
                                    </div>
                                    <div class="form-group row">
                                        <label
                                            class="col-sm-4 col-form-label">{{trans('forms.occupationProfile')}}</label>
                                        <div class="col-sm-8">
                                            {!! Form::text('occupation', null, array('placeholder' => 'Occupation','class' => 'form-control form-control-sm')) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="card-footer">
                            <button type="submit" class="btn btn-info btn-block"><i class="fas fa-save"></i> Submit
                            </button>
                        </div>
                    </div>
                    {!! Form::close() !!}
                @else
                    <p>{{ trans('profile.notYourProfile') }}</p>
                @endif
            @else
                <p>{{ trans('profile.noProfileYet') }}</p>
            @endif
        </div>
    </div>
@endsection
@section('third_party_scripts')
    @include('scripts.user-avatar-dz')
@stop


@extends('layouts.app')

@section('content')
    <div class="card">
        <h5 class="card-header">
            <i class='fas fa-user mr-2'></i>{{ trans('profile.templateTitle') }}
            <div class="float-right">
                <a class="btn btn-outline-warning btn-sm" href="{{ route('profile.index', $user->username) }}"> {!! trans('buttons.buttons.back') !!}</a>
            </div>
        </h5>
        <div class="card-body">
            @if ($user->profile)
                @if (Auth::user()->id == $user->id)
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-lg-12 col-sm-8 col-md-9">

                                <!-- Nav tabs -->
                                <ul class="nav nav-pills nav-justified" id="myTab" role="tablist">
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link active" href=".edit-profile-tab"
                                           data-toggle="pill" role="tab" aria-controls="edit-profile-tab"
                                           aria-selected="true">{{trans('profile.editProfileTitle')}}</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href=".edit-settings-tab" data-toggle="pill"
                                           role="tab" aria-controls="edit-settings-tab"
                                           aria-selected="false">{{trans('profile.editAccountTitle')}}</a>
                                    </li>
                                    <li class="nav-item" role="presentation">
                                        <a class="nav-link" href=".edit-account-tab" data-toggle="pill"
                                           role="tab" aria-controls="edit-settings-tab"
                                           aria-selected="false">{{trans('profile.editAccountAdminTitle')}}</a>
                                    </li>
                                </ul>

                                <!-- Tab panes -->
                                <div class="card mt-2">
                                    <div class="card-body">
                                        <div class="tab-content" id="v-pills-tabContent">
                                            <div class="tab-pane fade show active edit-profile-tab" role="tabpanel" aria-labelledby="edit-profile-tab">
                                                <div class="row mb-2">
                                                    <div class="col-sm-12">
                                                        <div id="avatar_container">
                                                            <div class="collapseOne card-collapse collapse @if($user->profile->avatar_status == 0) show @endif">
                                                                <div class="card-body">
                                                                    <div class="user-avatar text-center">
                                                                        <i class="fas fa-user-circle fa-4x"></i>
                                                                        <p>{{ $user->username }}</p>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="collapseTwo card-collapse collapse @if($user->profile->avatar_status == 1) show @endif">
                                                                <div class="card-body">
                                                                    <div class="dz-preview"></div>
                                                                    {!! Form::open(array('route' => 'avatar.upload', 'method' => 'POST', 'name' => 'avatarDropzone','id' => 'avatarDropzone', 'class' => 'form single-dropzone dropzone single', 'files' => true)) !!}
                                                                    <img id="user_selected_avatar"
                                                                         class="user-avatar"
                                                                         src="@if ($user->profile->avatar != null) {{ $user->profile->avatar }} @endif"
                                                                         alt="{{ $user->username }}">
                                                                    {!! Form::close() !!}
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                {!! Form::model($user->profile, ['method' => 'PATCH', 'route' => ['profile.update', $user->username], 'id' => 'user_profile_form', 'class' => 'form-horizontal', 'role' => 'form', 'enctype' => 'multipart/form-data']) !!}
                                                {{ csrf_field() }}
                                                <div class="row">
                                                    <div class="col-10 offset-1 col-sm-10 offset-sm-1 mb-4">
                                                        <div class="row">
                                                            <div class="col-6 col-xs-6">
                                                                <div class="btn-group-toggle" data-toggle="buttons">
                                                                    <label class="btn btn-outline-info @if($user->profile->avatar_status == 0) active @endif btn-block btn-sm"
                                                                           data-toggle="collapse" data-target=".collapseOne:not(.show), .collapseTwo.show">
                                                                        <input type="radio" name="avatar_status" id="option1" autocomplete="off" value="0"
                                                                               @if($user->profile->avatar_status == 0) checked @endif> Use Default
                                                                    </label>
                                                                </div>
                                                            </div>
                                                            <div class="col-6 col-xs-6">
                                                                <div class="btn-group-toggle" data-toggle="buttons">
                                                                    <label class="btn btn-outline-info @if($user->profile->avatar_status == 1) active @endif btn-block btn-sm"
                                                                           data-toggle="collapse" data-target=".collapseOne.show, .collapseTwo:not(.show)">
                                                                        <input type="radio" name="avatar_status" id="option2" autocomplete="off" value="1"
                                                                               @if($user->profile->avatar_status == 1) checked @endif> Use My Image
                                                                    </label>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="col-12">
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">{{trans('profile.showProfilemykadProfile')}}</label>
                                                        <div class="col-sm-10">
                                                            {!! Form::text('icnumber', null, array('placeholder' => trans('profile.showProfilemykadProfile'),'class' => 'form-control form-control-sm')) !!}
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">{{trans('profile.showProfilePhone')}}</label>
                                                        <div class="col-sm-10">
                                                            {!! Form::text('phone', null, array('placeholder' => trans('profile.showProfilePhone'),'class' => 'form-control form-control-sm')) !!}
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">{{trans('profile.showAgeProfile')}}</label>
                                                        <div class="col-sm-10">
                                                            {!! Form::text('age', null, array('placeholder' => trans('profile.showAgeProfile'),'class' => 'form-control form-control-sm')) !!}
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">{{trans('profile.showGenderProfile')}}</label>
                                                        <div class="col-sm-10">
                                                            {!! Form::text('gender', null, array('placeholder' => trans('profile.showGenderProfile'),'class' => 'form-control form-control-sm')) !!}
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">{{trans('profile.showRaceProfile')}}</label>
                                                        <div class="col-sm-10">
                                                            {!! Form::text('race', null, array('placeholder' => trans('profile.showRaceProfile'),'class' => 'form-control form-control-sm')) !!}
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">{{trans('profile.showOccupationProfile')}}</label>
                                                        <div class="col-sm-10">
                                                            {!! Form::text('occupation', null, array('placeholder' => trans('profile.showOccupationProfile'),'class' => 'form-control form-control-sm')) !!}
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">{{trans('profile.showAddressProfile')}}</label>
                                                        <div class="col-sm-10">
                                                            {!! Form::text('address', null, array('placeholder' => trans('profile.showAddressProfile'),'class' => 'form-control form-control-sm')) !!}
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label
                                                            class="col-sm-2 col-form-label">{{trans('profile.showPostcodeProfile')}}</label>
                                                        <div class="col-sm-10">
                                                            {!! Form::text('postcode', null, array('placeholder' => trans('profile.showPostcodeProfile'),'class' => 'form-control form-control-sm')) !!}
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">{{trans('profile.showStateProfile')}}</label>
                                                        <div class="col-sm-10">
                                                            {!! Form::text('state', null, array('placeholder' => trans('profile.showStateProfile'),'class' => 'form-control form-control-sm')) !!}
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label class="col-sm-2 col-form-label">{{trans('profile.showCountryProfile')}}</label>
                                                        <div class="col-sm-10">
                                                            {!! Form::text('country', null, array('placeholder' => trans('profile.showCountryProfile'),'class' => 'form-control form-control-sm')) !!}
                                                        </div>
                                                    </div>
                                                </div>

                                                <div class="form-group margin-bottom-2">
                                                    <div class="col-12">
                                                        {!! Form::button(
                                                            '<i class="fa fa-fw fa-save" aria-hidden="true"></i> ' . trans('profile.submitButton'),
                                                             array(
                                                                'id'                => 'confirmFormSave',
                                                                'class'             => 'btn btn-block btn-success disabled btn-sm',
                                                                'type'              => 'button',
                                                                'data-target'       => '#confirmForm',
                                                                'data-modalClass'   => 'modal-success',
                                                                'data-toggle'       => 'modal',
                                                                'data-title'        => trans('modals.edit_user__modal_text_confirm_title'),
                                                                'data-message'      => trans('modals.edit_user__modal_text_confirm_message')
                                                        )) !!}
                                                    </div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>

                                            <div class="tab-pane fade edit-settings-tab" role="tabpanel" aria-labelledby="edit-settings-tab">
                                                {!! Form::model($user, ['method' => 'PUT', 'route' => ['profile.updateUserAccount', $user->id], 'id' => 'user_basics_form', 'class' => 'form-horizontal', 'role' => 'form']) !!}
                                                {!! csrf_field() !!}
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label">{{trans('profile.showProfileUsername')}}</label>
                                                    <div class="col-sm-10">
                                                        {!! Form::text('username', null, array('placeholder' => trans('profile.showProfileUsername'),'class' => 'form-control form-control-sm')) !!}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label">{{trans('profile.showProfileFullName')}}</label>
                                                    <div class="col-sm-10">
                                                        {!! Form::text('fullname', null, array('placeholder' => trans('profile.showProfileFullName'),'class' => 'form-control form-control-sm')) !!}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label class="col-sm-2 col-form-label">{{trans('profile.showProfileEmail')}}</label>
                                                    <div class="col-sm-10">
                                                        {!! Form::text('email', null, array('placeholder' => trans('profile.showProfileEmail'),'class' => 'form-control form-control-sm')) !!}
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        {!! Form::button(
                                                            '<i class="fa fa-fw fa-save" aria-hidden="true"></i> ' . trans('profile.submitProfileButton'),
                                                             array(
                                                                'class'             => 'btn btn-block btn-success disabled btn-sm',
                                                                'id'                => 'account_save_trigger',
                                                                'disabled'          => true,
                                                                'type'              => 'button',
                                                                'data-submit'       => trans('profile.submitProfileButton'),
                                                                'data-target'       => '#confirmForm',
                                                                'data-modalClass'   => 'modal-success',
                                                                'data-toggle'       => 'modal',
                                                                'data-title'        => trans('modals.edit_user__modal_text_confirm_title'),
                                                                'data-message'      => trans('modals.edit_user__modal_text_confirm_message')
                                                        )) !!}
                                                    </div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>

                                            <div class="tab-pane fade edit-account-tab" role="tabpanel" aria-labelledby="edit-account-tab">
                                                {!! Form::model($user, ['method' => 'PUT', 'route' => ['profile.updateUserPassword', $user->id], 'autocomplete' => 'new-password', 'class' => 'form-horizontal', 'role' => 'form']) !!}

                                                <div class="pw-change-container mb-2">
                                                    <div class="form-group has-feedback row {{ $errors->has('password') ? ' has-error ' : '' }}">
                                                        {!! Form::label('password', trans('forms.create_user_label_password'), array('class' => 'col-md-2 control-label')); !!}
                                                        <div class="col-md-10">
                                                            {!! Form::password('password', array('id' => 'password', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_password'), 'autocomplete' => 'new-password')) !!}
                                                            @if ($errors->has('password'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('password') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>

                                                    <div class="form-group has-feedback row {{ $errors->has('password_confirmation') ? ' has-error ' : '' }}">
                                                        {!! Form::label('password_confirmation', trans('forms.create_user_label_pw_confirmation'), array('class' => 'col-md-2 control-label')); !!}
                                                        <div class="col-md-10">
                                                            {!! Form::password('password_confirmation', array('id' => 'password_confirmation', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_user_ph_pw_confirmation'))) !!}
                                                            <span id="pw_status"></span>
                                                            @if ($errors->has('password_confirmation'))
                                                                <span class="help-block">
                                                                    <strong>{{ $errors->first('password_confirmation') }}</strong>
                                                                </span>
                                                            @endif
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <div class="col-md-12">
                                                        {!! Form::button(
                                                            '<i class="fa fa-fw fa-save" aria-hidden="true"></i> ' . trans('profile.submitPWButton'),
                                                             array(
                                                                'class'             => 'btn btn-block btn-warning btn-sm',
                                                                'id'                => 'pw_save_trigger',
                                                                'disabled'          => true,
                                                                'type'              => 'button',
                                                                'data-submit'       => trans('profile.submitButton'),
                                                                'data-target'       => '#confirmForm',
                                                                'data-modalClass'   => 'modal-warning',
                                                                'data-toggle'       => 'modal',
                                                                'data-title'        => trans('modals.edit_user__modal_text_confirm_title'),
                                                                'data-message'      => trans('modals.edit_user__modal_text_confirm_message')
                                                        )) !!}
                                                    </div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @else
                    <div class="alert alert-warning" role="alert">
                        {{ trans('profile.notYourProfile') }}
                    </div>
                @endif
            @else
                <div class="alert alert-info" role="alert">
                    {{ trans('profile.noProfileYet') }}
                </div>
            @endif
        </div>
    </div>
    @include('modals.modal-form')
@endsection

@section('third_party_scripts')
    @include('scripts.form-modal-script')
    @include('scripts.user-avatar-dz')

    <script type="text/javascript">
        $(function() {
            $('#user_profile_form').on('keyup change', 'input, select, textarea', function() {
                $('#confirmFormSave').attr('disabled', false).removeClass('disabled').show();
            });
            $('#user_basics_form').on('keyup change', 'input, select, textarea', function() {
                $('#account_save_trigger').attr('disabled', false).removeClass('disabled').show();
            });

            $('#password_confirmation').keyup(function() {
                checkPasswordMatch();
            });

            $('#password, #password_confirmation').keyup(function() {
                enableSubmitPWCheck();
            });

            $('#password, #password_confirmation').hideShowPassword(false);

            $('#password').password({
                shortPass: 'The password is too short',
                badPass: 'Weak - Try combining letters & numbers',
                goodPass: 'Medium - Try using special charecters',
                strongPass: 'Strong password',
                containsUsername: 'The password contains the username',
                enterPass: false,
                showPercent: false,
                showText: true,
                animate: true,
                animateSpeed: 50,
                username: false, // select the username field (selector or jQuery instance) for better password checks
                usernamePartialMatch: true,
                minimumLength: 6,
            });

            function checkPasswordMatch() {
                var password = $('#password').val();
                var confirmPassword = $('#password_confirmation').val();
                if (password !== confirmPassword) {
                    $('#pw_status').html('Passwords do not match!');
                } else {
                    $('#pw_status').html('Passwords match.');
                }
            }

            function enableSubmitPWCheck() {
                var password = $('#password').val();
                var confirmPassword = $('#password_confirmation').val();
                var submitChange = $('#pw_save_trigger');
                if (password !== confirmPassword) {
                    submitChange.attr('disabled', true);
                } else {
                    submitChange.attr('disabled', false);
                }
            }
        });
    </script>
@endsection


<script src="{{asset('/plugins/DataTables/datatables.js')}}"></script>
<script src="{{asset('/plugins/DataTables/DataTables-1.10.25/js/dataTables.dataTables.min.js')}}"></script>
<script src="{{asset('/plugins/DataTables/DataTables-1.10.25/js/dataTables.bootstrap4.js')}}"></script>

<script type="text/javascript">
    $(function() {

        var a = $('#userList').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': false,
            'info': true,
            'autoWidth': false,
            'sPaginationType': 'full_numbers',
            'columnDefs': [
                {'searchable': false, 'targets': [0, 4]},
                {'width': '5%', 'targets': [0]},
                {'width': '20%', 'targets': [4]},
            ],
        });
        a.on('order.dt search.dt', function() {
            a.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        var b = $('#rolesList').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': false,
            'info': true,
            'autoWidth': false,
            'sPaginationType': 'full_numbers',
            'columnDefs': [
                {'searchable': false, 'targets': [0, 2]},
                {'width': '5%', 'targets': [0]},
                {'width': '20%', 'targets': [2]},
            ],
        });
        b.on('order.dt search.dt', function() {
            b.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        var c = $('#butikList').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': false,
            'info': true,
            'autoWidth': false,
            'sPaginationType': 'full_numbers',
            'columnDefs': [
                {'searchable': false, 'targets': [0, 5]},
                {'width': '5%', 'targets': [0]},
                {'width': '20%', 'targets': [2]},
            ],
        });
        c.on('order.dt search.dt', function() {
            c.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        var d = $('#tablePerunding').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': false,
            'ordering': false,
            'info': true,
            'autoWidth': false,
            'sPaginationType': 'simple',
        });
        d.on('order.dt', function() {
            d.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();

        var e = $('#tableJuruterapi').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': false,
            'ordering': false,
            'info': true,
            'autoWidth': false,
            'sPaginationType': 'simple',
        });
        e.on('order.dt', function() {
            e.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();



        var f = $('#patientTreatmentList').DataTable({
            'paging': true,
            'lengthChange': true,
            'searching': true,
            'ordering': false,
            'info': true,
            'autoWidth': false,
            'sPaginationType': 'simple',
        });
        f.on('order.dt', function() {
            f.column(0, {search: 'applied', order: 'applied'}).nodes().each(function(cell, i) {
                cell.innerHTML = i + 1;
            });
        }).draw();
    });
</script>

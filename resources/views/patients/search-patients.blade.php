@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <h5 class="card-header">
                    <i class="fas fa-search mr-2"></i> {!! trans('patient.search-all-patients') !!}
                </h5>
                <div class="card-body">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-7 offset-md-3">
                                {!! Form::open(array('route' => 'patients.search', 'method' => 'POST', 'role' => 'search','class' => 'form-inline')) !!}
                                {{ csrf_field() }}

                                {!! Form::label('searchIC', trans('forms.create_patient_label_ic'), array('class' => 'control-label col-form-label-sm mr-sm-2')); !!}
                                {!! Form::text('searchIC', null, array('id' => 'searchIC', 'class' => 'form-control col-md-6 form-control-sm mr-sm-2', 'placeholder' => trans('forms.create_patient_ph_ic'),'autofocus')) !!}

                                <button class="btn btn-sm btn-info hvr-glow mr-sm-2" type="submit">
                                    <i class="fa fa-search" aria-hidden="true"></i> {{trans('forms.search_patient_button_text')}}
                                </button>
                                <a class="btn btn-sm btn-secondary hvr-glow" href="{{ url('/patients') }}" role="button">
                                    <i class="fa fa-times" aria-hidden="true"></i>{{trans('forms.search_clear_patient_button_text')}}
                                </a>
                                {!!Form::Close()!!}
                            </div>
                        </div>
                    </div>
                    @if(isset($searchresult))
                        <p></p>
                        <div class="table-responsive-sm">
                            <table class="table table-bordered table-sm">
                                <thead class="text-center text-capitalize table-dark">
                                <tr>
                                    <th scope="col" colspan="7" class="align-middle">{{trans('forms.search-info')}} : {{$searchIC}}</th>
                                </tr>
                                <tr>
                                    <th scope="col" class="align-middle">{{trans('forms.create_user_label_fullname')}}</th>
                                    <th scope="col" class="align-middle">{{trans('forms.create_user_label_icnumber')}}</th>
                                    <th scope="col" class="align-middle">{{trans('forms.create_patient_label_phone')}}</th>
                                    <th scope="col" class="align-middle">{{trans('forms.dateRegister')}}</th>
                                    <th scope="col" colspan="2" class="align-middle">{{trans('forms.row_action')}}</th>
                                </tr>
                                </thead>
                                <tbody class="text-center">
                                @if($countSearch == 0)
                                    <tr class="table-danger">
                                        <td colspan="7" class="align-middle"><span class="badge badge-danger">{{trans('forms.noData')}}</span></td>
                                    </tr>
                                    @can('patient-create')
                                        <tr class="table-danger">
                                            <td colspan="7" class="align-middle">
                                                <a class="btn btn-sm btn-warning hvr-glow" href="{{ route('patients.create') }}">
                                                    <i class="fa fa-plus-circle mr-2" aria-hidden="true"></i>{!! trans('titles.preReg') !!}
                                                </a>
                                            </td>
                                        </tr>
                                    @endcan
                                @endif
                                @foreach($searchresult AS $row)
                                    <tr>
                                        <td class="align-middle">{{$row->full_name}}</td>
                                        <td class="align-middle">{{$row->ic_number}}</td>
                                        <td class="align-middle">{{$row->phone_number}}</td>
                                        <td class="align-middle">{{$row->created_at}}</td>
                                        <td class="align-middle">
                                            <a class="btn btn-sm btn-info hvr-glow" href="{{ route('patients.show',$row->id) }}" data-toggle="tooltip" title="Show">
                                                {!! trans('patient.buttons.show') !!}
                                            </a>
                                            <a class="btn btn-sm btn-primary hvr-glow" href="{{ route('treatment.patient',$row->id) }}" data-toggle="tooltip" title="Treatment">
                                                {!! trans('patient.buttons.treatment-patient') !!}
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>

    @include('modals.modal-delete')

@endsection

@section('footer_scripts')
    @include('scripts.delete-modal-script')
@endsection

@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <h5 class="card-header text-uppercase">
                    {{ $patients->full_name }}'s {{trans('profile.showProfileTitle')}}
                    <div class="float-right">
                        <a class="btn btn-outline-warning btn-sm" href="{{ route('patients.index') }}"> {!! trans('buttons.buttons.back') !!}</a>
                    </div>
                </h5>
                <div class="card-body">
                    <div class="row">
                        <div class="col-2 text-center align-middle">
                            @if(!empty($patients->image))
                                <img src="{{$butik->image}}" alt="{{ $butik->image }}"
                                     class="butik-avatar-show rounded border border-dark">
                            @else
                                <i class="fas fa-user-tie fa-9x"></i>
                            @endif
                        </div>
                        <div class="col-5">
                            <dl>
                                <dt class="col-sm-4">{{trans('profile.showProfileMrn')}}</dt>
                                <dd class="col-sm-8"><label class="badge badge-primary">{{ $patients->patientMRN }}</label></dd>
                                <dt class="col-sm-4">{{trans('profile.showProfileFullName')}}</dt>
                                <dd class="col-sm-8">{{ $patients->full_name }}</dd>
                                <dt class="col-sm-4">{{trans('profile.showProfilemykadProfile')}}</dt>
                                <dd class="col-sm-8">
                                    @if ($patients->ic_number)
                                        {{ $patients->ic_number }}
                                    @else
                                        <label class="badge badge-warning">{{trans('forms.span_none')}}</label>
                                    @endif
                                </dd>
                                <dt class="col-sm-4">{{trans('profile.showAgeProfile')}}</dt>
                                <dd class="col-sm-8">
                                    @if ($patientInfos->age)
                                        {{ $patientInfos->age }}
                                    @else
                                        <label class="badge badge-warning">{{trans('forms.span_none')}}</label>
                                    @endif
                                </dd>
                                <dt class="col-sm-4">{{trans('profile.showReligionProfile')}}</dt>
                                <dd class="col-sm-8">
                                    @if ($patients->religion)
                                        {{ $patients->religion }}
                                    @else
                                        <label class="badge badge-warning">{{trans('forms.span_none')}}</label>
                                    @endif
                                </dd>
                                <dt class="col-sm-4">{{trans('profile.showProfileEmail')}}</dt>
                                <dd class="col-sm-8">
                                    @if ($patientInfos->email)
                                        {{ $patientInfos->email }}
                                    @else
                                        <label class="badge badge-warning">{{trans('forms.span_none')}}</label>
                                    @endif
                                </dd>
                                <dt class="col-sm-4">{{trans('profile.showProfilePhone')}}</dt>
                                <dd class="col-sm-8">
                                    @if ($patients->phone_number)
                                        {{ $patients->phone_number }}
                                    @else
                                        <label class="badge badge-warning">{{trans('forms.span_none')}}</label>
                                    @endif
                                </dd>
                                <dt class="col-sm-4">{{trans('profile.showProfileRegistrationDate')}}</dt>
                                <dd class="col-sm-8">
                                    @if ($patients->created_at)
                                        {{ $patients->created_at }}
                                    @else
                                        <label class="badge badge-warning">{{trans('forms.span_none')}}</label>
                                    @endif
                                </dd>
                            </dl>
                        </div>
                        <div class="col-5">
                            <dl>
                                <dt class="col-sm-4">{{trans('profile.showRaceProfile')}}</dt>
                                <dd class="col-sm-8">
                                    @if ($patientInfos->race)
                                        {{ $patientInfos->race }}
                                    @else
                                        <label class="badge badge-warning">{{trans('forms.span_none')}}</label>
                                    @endif
                                </dd>
                                <dt class="col-sm-4">{{trans('profile.showGenderProfile')}}</dt>
                                <dd class="col-sm-8">
                                    @if ($patientInfos->gender)
                                        {{ $patientInfos->gender }}
                                    @else
                                        <label class="badge badge-warning">{{trans('forms.span_none')}}</label>
                                    @endif
                                </dd>
                                <dt class="col-sm-4">{{trans('profile.showMaritalProfile')}}</dt>
                                <dd class="col-sm-8">
                                    @if ($patientInfos->marital)
                                        {{ $patientInfos->marital }}
                                    @else
                                        <label class="badge badge-warning">{{trans('forms.span_none')}}</label>
                                    @endif
                                </dd>
                                <dt class="col-sm-4">{{trans('profile.showOccupationProfile')}}</dt>
                                <dd class="col-sm-8">
                                    @if ($patientInfos->occupation)
                                        {{ $patientInfos->occupation }}
                                    @else
                                        <label class="badge badge-warning">{{trans('forms.span_none')}}</label>
                                    @endif
                                </dd>
                                <dt class="col-sm-4">{{trans('profile.showCitizenshipProfile')}}</dt>
                                <dd class="col-sm-8">
                                    @if ($patientInfos->citizenship)
                                        @if($patientInfos->citizenship == 1)
                                            <label class="badge badge-info">{{trans('forms.citizen')}}</label>
                                        @else
                                            <label class="badge badge-info">{{trans('forms.nonCitizen')}}</label>
                                        @endif
                                    @else
                                        <label class="badge badge-warning">{{trans('forms.span_none')}}</label>
                                    @endif
                                </dd>
                                <dt class="col-sm-4">{{trans('profile.showAddressProfile')}}</dt>
                                <dd class="col-sm-8">
                                    @if ($patientInfos->address || $patientInfos->postcode || $patientInfos->city || $patientInfos->state)
                                        {{ $patientInfos->address }},
                                        {{ $patientInfos->postcode }},
                                        {{ $patientInfos->city }},
                                        {{ $patientInfos->state }}
                                    @else
                                        <label class="badge badge-warning">{{trans('forms.span_none')}}</label>
                                    @endif
                                </dd>
                                <dt class="col-sm-4">{{trans('profile.showNationalityProfile')}}</dt>
                                <dd class="col-sm-8">
                                    @if ($patientInfos->nationality)
                                        <label class="badge badge-info">{{ $patientInfos->nationality }}</label>
                                    @else
                                        <label class="badge badge-warning">{{trans('forms.span_none')}}</label>
                                    @endif
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
@endsection

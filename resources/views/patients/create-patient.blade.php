@extends('layouts.app')

@section('content')
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <h5 class="card-header">
                    <i class="fas fa-hospital-user mr-2"></i> {!! trans('patient.pre-reg-patient') !!}
                </h5>

                <div class="card-body">
                    {!! Form::open(array('route' => 'patients.store', 'method' => 'POST', 'role' => 'form', 'class' => 'needs-validation')) !!}
                    {!! csrf_field() !!}
                    <div class="form-group has-feedback row {{ $errors->has('patientName') ? ' has-error ' : '' }}">
                        {!! Form::label('patientName', trans('forms.create_patient_label_name'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                        <div class="col-md-10">
                            {!! Form::text('patientName', null, array('id' => 'patientName', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_patient_ph_name'),'autofocus')) !!}
                            @if ($errors->has('patientName'))
                                <small class="form-text text-danger">
                                    {{ $errors->first('patientName') }}
                                </small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group has-feedback row {{ $errors->has('patientIc') ? ' has-error ' : '' }}">
                        {!! Form::label('patientIc', trans('forms.create_patient_label_ic'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                        <div class="col-md-10">
                            {!! Form::text('patientIc', null, array('id' => 'patientIc', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_patient_ph_ic'))) !!}

                            @if ($errors->has('patientIc'))
                                <small class="form-text text-danger">
                                    {{ $errors->first('patientIc') }}
                                </small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group has-feedback row {{ $errors->has('patientPhone') ? ' has-error ' : '' }}">
                        {!! Form::label('patientPhone', trans('forms.create_patient_label_phone'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                        <div class="col-md-10">
                            {!! Form::text('patientPhone', null, array('id' => 'patientPhone', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_patient_ph_phone'))) !!}
                            @if ($errors->has('patientPhone'))
                                <small class="form-text text-danger">
                                    {{ $errors->first('patientPhone') }}
                                </small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group has-feedback row {{ $errors->has('patientCitizen') ? ' has-error ' : '' }}">
                        {!! Form::label('patientCitizen', trans('forms.create_patient_label_citizen'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                        <div class="col-md-10">
                            <select class="form-control form-control-sm" name="patientCitizen" id="patientCitizen">
                                <option value="0" @if(old('patientCitizen') == 0) selected="selected" @endif>{{trans('forms.nonCitizen')}}</option>
                                <option value="1" @if(old('patientCitizen') == 1) selected="selected" @endif>{{trans('forms.citizen')}}</option>
                            </select>
                            @if ($errors->has('patientCitizen'))
                                <small class="form-text text-danger">
                                    {{ $errors->first('patientCitizen') }}
                                </small>
                            @endif
                        </div>
                    </div>
                    <div class="form-group has-feedback row {{ $errors->has('patientNationality') ? ' has-error ' : '' }}">
                        {!! Form::label('patientNationality', trans('forms.create_patient_label_nationality'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                        <div class="col-md-10">
                            <select class="form-control form-control-sm" name="patientNationality" id="patientNationality">
                                @foreach($countries AS $rowCountry)
                                    <option value="{{$rowCountry->ccode}}" @if(old('patientNationality') == $rowCountry->ccode) selected="selected" @endif>{{$rowCountry->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('patientNationality'))
                                <small class="form-text text-danger">
                                    {{ $errors->first('patientNationality') }}
                                </small>
                            @endif
                        </div>
                    </div>
                    <div class="card border-primary">
                        <div class="card-body">
                            <div class="form-group has-feedback row {{ $errors->has('patientAddress') ? ' has-error ' : '' }}">
                                {!! Form::label('patientAddress', trans('forms.create_patient_label_address'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                                <div class="col-md-10">
                                    {!! Form::text('patientAddress', null, array('id' => 'patientAddress', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_patient_ph_address'))) !!}

                                    @if ($errors->has('patientAddress'))
                                        <small class="form-text text-danger">
                                            {{ $errors->first('patientAddress') }}
                                        </small>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('patientPostcode') ? ' has-error ' : '' }}">
                                {!! Form::label('patientPostcode', trans('forms.create_patient_label_postal'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                                <div class="col-md-10">
                                    {!! Form::text('patientPostcode', null, array('id' => 'patientPostcode', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_patient_ph_postal'))) !!}

                                    @if ($errors->has('patientPostcode'))
                                        <small class="form-text text-danger">
                                            {{ $errors->first('patientPostcode') }}
                                        </small>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('patientCity') ? ' has-error ' : '' }}">
                                {!! Form::label('patientCity', trans('forms.create_patient_label_city'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                                <div class="col-md-10">
                                    {!! Form::text('patientCity', null, array('id' => 'patientCity', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_patient_ph_city'))) !!}

                                    @if ($errors->has('patientCity'))
                                        <small class="form-text text-danger">
                                            {{ $errors->first('patientCity') }}
                                        </small>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group has-feedback row {{ $errors->has('patientState') ? ' has-error ' : '' }}">
                                {!! Form::label('patientState', trans('forms.create_patient_label_state'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                                <div class="col-md-10">
                                    <select class="form-control form-control-sm" name="patientState" id="patientState">
                                        @foreach($state AS $rowState)
                                            <option value="{{$rowState->name}}" @if(old('patientState') == $rowState->name) selected="selected" @endif>{{$rowState->name}}</option>
                                        @endforeach
                                    </select>
                                    @if ($errors->has('patientState'))
                                        <small class="form-text text-danger">
                                            {{ $errors->first('patientState') }}
                                        </small>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                    {!! Form::button(trans('forms.create_patient_button_text'), array('class' => 'btn btn-sm btn-success btn-block margin-bottom-1 mb-1 float-right hvr-glow','type' => 'submit' )) !!}

                    {!! Form::Close() !!}
                </div>

            </div>
        </div>
    </div>
@endsection

@section('footer_scripts')
@endsection

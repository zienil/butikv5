<button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar" data-class="c-sidebar-show">
    <i class="c-icon c-icon-lg cil-menu"></i>
</button>
<a class="c-header-brand d-lg-none" href="#">
    <i class="c-icon c-icon-lg cib-full"></i>
</a>
<button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar" data-class="c-sidebar-lg-show" responsive="true">
    <i class="c-icon c-icon-lg cil-menu"></i>
</button>

<ul class="c-header-nav mfs-auto">
</ul>

<ul class="c-header-nav ml-auto">
    <li class="c-header-nav-item dropdown">
        <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
           aria-expanded="false">
            <div class="c-avatar">
                @if ((Auth::User()->profile) && Auth::user()->profile->avatar_status == 1)
                    @if(!empty(Auth::user()->profile->avatar))
                        <img src="{{ Auth::user()->profile->avatar }}" alt="{{ Auth::user()->username }}"
                             class="c-avatar-img user-avatar-nav border border-dark">
                    @else
                        <div class="c-avatar-img user-avatar-nav"><i class="fas fa-user-circle fa-2x pl-2"></i></div>
                    @endif
                @else
                    <div class="c-avatar-img user-avatar-nav"><i class="fas fa-user-circle fa-2x pl-2"></i></div>
                @endif
            </div>
        </a>

        <div class="dropdown-menu dropdown-menu-right pt-0">
            <div class="dropdown-header bg-light py-2 text-center"><strong>{{Auth::user()->username}}</strong></div>
            <a class="dropdown-item" href="{{ route('profile.index',Auth::user()->username) }}">
                <i class="c-icon mfe-2 cil-user"></i> {{trans('navbar.profile')}}
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                <i class="cil-account-logout mr-2"></i> {{trans('navbar.logout')}}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    </li>
</ul>

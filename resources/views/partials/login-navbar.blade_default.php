<ul class="c-header-nav ml-auto mr-4">
    <li><a class="nav-link" href="{{ route('login') }}">{{ trans('lang.login') }}</a></li>
    @if (Route::has('register'))
        <li><a class="nav-link" href="{{ route('register') }}">{{ trans('lang.register') }}</a></li>
    @endif
</ul>

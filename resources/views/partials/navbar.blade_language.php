<button class="c-header-toggler c-class-toggler d-lg-none mfe-auto" type="button" data-target="#sidebar"
        data-class="c-sidebar-show">
    <i class="cil-menu"></i>
</button>
<a class="c-header-brand d-lg-none" href="#">
    <i class="c-icon c-icon-lg cib-full"></i>
</a>
<button class="c-header-toggler c-class-toggler mfs-3 d-md-down-none" type="button" data-target="#sidebar"
        data-class="c-sidebar-lg-show" responsive="true">
    <i class="c-icon c-icon-lg cil-menu"></i>
</button>
<ul class="c-header-nav d-md-down-none">
    <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown"
           aria-haspopup="true" aria-expanded="false">
            <span class="flag-icon flag-icon-{{Config::get('languages')[App::getLocale()]['flag-icon']}} mr-1"></span>
            {{ Config::get('languages')[App::getLocale()]['display'] }}
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            @foreach (Config::get('languages') as $lang => $language)
                @if ($lang != App::getLocale())
                    <a class="dropdown-item" href="{{ route('lang.switch', $lang) }}">
                        <span class="flag-icon flag-icon-{{$language['flag-icon']}} mr-1"></span> {{$language['display']}}
                    </a>
                @endif
            @endforeach
        </div>
    </li>
</ul>

<ul class="c-header-nav ml-auto mr-4">
    @guest
        <li><a class="nav-link" href="{{ route('login') }}">{{ trans('titles.login') }}</a></li>
        @if (Route::has('register'))
            <li><a class="nav-link" href="{{ route('register') }}">{{ trans('titles.register') }}</a></li>
        @endif
    @else
        <a class="c-header-nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true"
           aria-expanded="false">
            <div class="c-avatar">
                {{ Auth::user()->username }}
            </div>
        </a>
        <div class="dropdown-menu dropdown-menu-right pt-0">
            <div class="dropdown-header bg-light py-2"><strong>Settings</strong></div>
            <a class="dropdown-item {{ Request::is('profile/'.Auth::user()->username, 'profile/'.Auth::user()->username . '/edit') ? 'active' : null }}"
               href="{{ url('/profile/'.Auth::user()->username) }}">
                <i class="cil-user mr-2"></i>{!! trans('titles.profile') !!}
            </a>
            <a class="dropdown-item" href="#"><i class="cil-settings mr-2"></i> Settings</a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item" href="{{ route('logout') }}"
               onclick="event.preventDefault();document.getElementById('logout-form').submit();">
                <i class="cil-account-logout mr-2"></i> {{ __('Logout') }}
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                @csrf
            </form>
        </div>
    @endguest
</ul>

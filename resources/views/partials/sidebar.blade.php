<div class="c-sidebar-brand d-lg-down-none">
    <div class="c-sidebar-brand-full">
        <i class="fa fa-heartbeat fa-3x" aria-hidden="true" style="color: red"></i>
    </div>
    <div class="c-sidebar-brand-minimized">
        <i class="fa fa-heartbeat fa-2x" aria-hidden="true" style="color: red"></i>
    </div>
</div>
<ul class="c-sidebar-nav">
    <li class="c-sidebar-nav-item">
        <a class="c-sidebar-nav-link" href="{{route('home')}}"> <i class="c-sidebar-nav-icon cil-speedometer"></i>Dashboard</a>
    </li>
    @if(Auth::user()->hasRole('System Admin'))
        <li class="c-sidebar-nav-title">{{trans('sidebar.adminDropdownNav')}}</li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ route('users.index') }}">
                <i class="c-sidebar-nav-icon cil-user"></i>{{trans('sidebar.adminUsers')}}
            </a>
        </li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ route('roles.index') }}">
                <i class="c-sidebar-nav-icon cil-shield-alt"></i>{{trans('sidebar.usersRole')}}
            </a>
        </li>
    @endif
    @if(Auth::user()->can('butik-list'))
        <li class="c-sidebar-nav-title">{{trans('sidebar.butik')}}</li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ route('butiks.index') }}">
                <i class="c-sidebar-nav-icon cil-hospital"></i>{{trans('sidebar.butik-list')}}
            </a>
        </li>
    @endif
    @if(Auth::user()->can('patient-list'))
        <li class="c-sidebar-nav-title">{{trans('sidebar.patients')}}</li>
        <li class="c-sidebar-nav-item">
            <a class="c-sidebar-nav-link" href="{{ route('patients.index') }}">
                <i class="c-sidebar-nav-icon cil-search"></i>{{trans('sidebar.patients-search')}}
            </a>
        </li>
        @can('patient-create')
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="{{ route('patients.create') }}">
                    <i class="c-sidebar-nav-icon cil-group"></i>{{trans('sidebar.patients-register')}}
                </a>
            </li>
        @endcan
    @endif
    <li class="c-sidebar-nav-title">Components</li>
    <li class="c-sidebar-nav-item c-sidebar-nav-dropdown">
        <a class="c-sidebar-nav-link c-sidebar-nav-dropdown-toggle" href="#">
            <i class="c-sidebar-nav-icon cil-cursor"></i> Buttons</a>
        <ul class="c-sidebar-nav-dropdown-items">
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="#"><span
                        class="c-sidebar-nav-icon"></span> Buttons</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="#"><span
                        class="c-sidebar-nav-icon"></span> Buttons Group</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="#"><span
                        class="c-sidebar-nav-icon"></span> Dropdowns</a></li>
            <li class="c-sidebar-nav-item"><a class="c-sidebar-nav-link" href="#"><span
                        class="c-sidebar-nav-icon"></span> Brand Buttons</a></li>
        </ul>
    </li>

    <li class="c-sidebar-nav-title  mt-auto">@lang('sidebar.language')</li>
    @foreach (Config::get('languages') as $lang => $language)
        @if ($lang != App::getLocale())
            <li class="c-sidebar-nav-item">
                <a class="c-sidebar-nav-link" href="{{ route('lang.switch', $lang) }}">
                    <i class="c-sidebar-nav-icon cif-{{$language['flag-icon']}}"></i>{{$language['display']}}
                </a>
            </li>
        @endif
    @endforeach
</ul>
<button class="c-sidebar-minimizer c-class-toggler" type="button" data-target="_parent"
        data-class="c-sidebar-minimized"></button>

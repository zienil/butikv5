@extends('layouts.app')


@section('content')
    <div class="card">
        <h5 class="card-header text-uppercase">{{trans('usersmanagement.showing-user')}} : <span class="text-uppercase">{{$user->username}}</span>
            <div class="float-right">
                <a class="btn btn-outline-warning btn-sm" href="{{ route('users.index') }}"> {!! trans('usersmanagement.buttons.back') !!}</a>
            </div>
        </h5>
        <div class="card-body">
            <dl class="row">
                <dt class="col-sm-3">{{trans('forms.create_user_label_username')}}</dt>
                <dd class="col-sm-9">{{ $user->username }}</dd>
                <dt class="col-sm-3">{{trans('forms.create_user_label_fullname')}}</dt>
                <dd class="col-sm-9">{{ $user->fullname }}</dd>
                <dt class="col-sm-3">{{trans('forms.create_user_label_email')}}</dt>
                <dd class="col-sm-9">{{ $user->email }}</dd>
                <dt class="col-sm-3">{{trans('forms.create_user_label_role')}}</dt>
                <dd class="col-sm-9">
                    @if(!empty($user->getRoleNames()))
                        @foreach($user->getRoleNames() as $v)
                            <label class="badge badge-success">{{ $v }}</label>
                        @endforeach
                    @endif
                </dd>
                <dt class="col-sm-3">{{trans('forms.create_clinic_label_name')}}</dt>
                <dd class="col-sm-9">
                    @if(!empty($userButik[0]['id']))
                        @foreach($butik as $row)
                            @if($userButik[0]['butik_id'] == $row->id)
                                <label class="badge badge-info">{{$row->butik_name}}</label>
                            @endif
                        @endforeach
                    @else
                        <label class="badge badge-warning">{{trans('forms.span_none')}}</label>
                    @endif
                </dd>
            </dl>
        </div>
    </div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="card">
        <h5 class="card-header text-uppercase">
            <i class='fas fa-users-cog mr-2'></i> {{trans('usersmanagement.userManagement')}}
        </h5>
        <div class="card-body">
            @if(Auth::user()->can('user-create'))
                <div class="float-right mb-2">
                    <a class="btn btn-sm btn-success"
                       href="{{ route('users.create') }}"> {!! trans('usersmanagement.buttons.create-new') !!}</a>
                </div>
            @endif
            <div class="table-responsive">
                <table id="userList" class="table table-sm table-bordered">
                    <thead class="thead-dark text-center">
                    <tr>
                        <th scope="col" class="align-middle">#</th>
                        <th scope="col" class="align-middle">{{trans('usersmanagement.users-table.fullname')}}</th>
                        <th scope="col" class="align-middle">{{trans('usersmanagement.users-table.email')}}</th>
                        <th scope="col" class="align-middle">{{trans('usersmanagement.users-table.role')}}</th>
                        <th scope="col" class="align-middle">{{trans('usersmanagement.users-table.actions')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($data as $key => $user)
                        <tr class="text-center">
                            <td class="align-middle"></td>
                            <td class="align-middle">{{ $user->fullname }}</td>
                            <td class="align-middle">{{ $user->email }}</td>
                            <td class="align-middle">
                                @if(!empty($user->getRoleNames()))
                                    @foreach($user->getRoleNames() as $v)
                                        <label class="badge badge-success">{{ $v }}</label>
                                    @endforeach
                                @endif
                            </td>
                            <td class="align-middle">
                                <a class="btn btn-sm btn-info" href="{{ route('users.show',$user->id) }}">{!! trans('usersmanagement.buttons.show') !!}</a>
                                @if(Auth::user()->can('user-edit'))
                                    <a class="btn btn-sm btn-primary" href="{{ route('users.edit',$user->id) }}">{!! trans('usersmanagement.buttons.edit') !!}</a>
                                @endif
                                @if(Auth::user()->can('user-delete'))
                                    {!! Form::open(array('route' => ['users.destroy', $user->id], 'title' => 'Delete','style'=>'display:inline')) !!}
                                    {!! Form::hidden('_method', 'DELETE') !!}
                                    {!! Form::button(trans('usersmanagement.buttons.delete'), array('class' => 'btn btn-danger btn-sm','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => 'Delete User', 'data-message' => trans('usersmanagement.modals.delete_user_message'))) !!}
                                    {!! Form::close() !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>

                </table>
            </div>
        </div>
    </div>
    @include('modals.modal-delete')
@endsection
@section('third_party_stylesheets')
    <link rel="stylesheet" href="{{asset('/plugins/DataTables/DataTables-1.10.25/css/dataTables.bootstrap4.min.css')}}"/>
@stop
@section('third_party_scripts')
    @include('scripts.datatables')
    @include('scripts.delete-modal-script')
@stop

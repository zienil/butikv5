@extends('layouts.app')


@section('content')
    <div class="card">
        <h5 class="card-header text-uppercase">{{trans('usersmanagement.editing-user')}} : {{$user->username}}
            <div class="float-right">
                <a class="btn btn-outline-warning btn-sm" href="{{ route('users.index') }}"> {!! trans('usersmanagement.buttons.back') !!}</a>
            </div>
        </h5>
        {!! Form::model($user, ['method' => 'PATCH','route' => ['users.update', $user->id]]) !!}
        <div class="card-body">
            @if (count($errors) > 0)
                <div class="alert alert-danger">
                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">{{trans('forms.create_user_label_username')}}</label>
                <div class="col-sm-10">
                    {!! Form::text('username', null, array('class' => 'form-control-plaintext','readonly')) !!}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">{{trans('forms.create_user_label_fullname')}}</label>
                <div class="col-sm-10">
                    {!! Form::text('fullname', null, array('placeholder' => trans('forms.create_user_ph_fullname'),'class' => 'form-control form-control-sm')) !!}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">{{trans('forms.create_user_label_email')}}</label>
                <div class="col-sm-10">
                    {!! Form::text('email', null, array('placeholder' => trans('forms.create_user_ph_email'),'class' => 'form-control form-control-sm')) !!}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">{{trans('forms.create_user_label_password')}}</label>
                <div class="col-sm-10">
                    {!! Form::password('password', array('placeholder' => trans('forms.create_user_ph_password'),'class' => 'form-control form-control-sm')) !!}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">{{trans('forms.create_user_label_pw_confirmation')}}</label>
                <div class="col-sm-10">
                    {!! Form::password('confirm-password', array('placeholder' => trans('forms.create_user_ph_pw_confirmation'),'class' => 'form-control form-control-sm')) !!}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">{{trans('forms.create_user_label_role')}}</label>
                <div class="col-sm-10">
                    {!! Form::select('roles', $roles,$userRole, array('class' => 'form-control form-control-sm')) !!}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">{{trans('forms.create_clinic_label_name')}}</label>
                <div class="col-sm-10">
                    <select class="form-control form-control-sm" name="butik" id="butik">
                        @if(!empty($userButik[0]['id']))
                            @if ($butik)
                                @foreach($butik as $row)
                                    <option value="{{ $row->id }}" {{ $userButik[0]['butik_id'] ==  $row->id ? 'selected="selected"' : '' }}>{{$row->butik_name}}</option>
                                @endforeach
                            @endif
                        @else
                            <option selected disabled value="">-- Choose --</option>
                            @foreach($butik as $row)
                                <option value="{{ $row->id }}">{{$row->butik_name}}</option>
                            @endforeach
                        @endif
                    </select>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-info btn-block">{!! trans('usersmanagement.buttons.update') !!}</button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@extends('layouts.app')

@section('content')
    <div class="card">
        <h5 class="card-header text-uppercase">{{trans('usersmanagement.create-new-user')}}
            <div class="float-right">
                <a class="btn btn-outline-warning btn-sm" href="{{ route('users.index') }}"> {!! trans('usersmanagement.buttons.back') !!}</a>
            </div>
        </h5>
        {!! Form::open(array('route' => 'users.store','method'=>'POST')) !!}
        <div class="card-body">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">{{trans('forms.create_user_label_username')}}</label>
                <div class="col-sm-10">
                    {!! Form::text('username', null, array('placeholder' => trans('forms.create_user_ph_username') ,'class' => 'form-control form-control-sm')) !!}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">{{trans('forms.create_user_label_fullname')}}</label>
                <div class="col-sm-10">
                    {!! Form::text('fullname', null, array('placeholder' => trans('forms.create_user_ph_fullname'),'class' => 'form-control form-control-sm')) !!}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">{{trans('forms.create_user_label_email')}}</label>
                <div class="col-sm-10">
                    {!! Form::text('email', null, array('placeholder' => trans('forms.create_user_ph_email'),'class' => 'form-control form-control-sm')) !!}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">{{trans('forms.create_user_label_password')}}</label>
                <div class="col-sm-10">
                    {!! Form::password('password', array('placeholder' => trans('forms.create_user_ph_password'),'class' => 'form-control form-control-sm')) !!}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">{{trans('forms.create_user_label_pw_confirmation')}}</label>
                <div class="col-sm-10">
                    {!! Form::password('confirm-password', array('placeholder' => trans('forms.create_user_ph_pw_confirmation'),'class' => 'form-control form-control-sm')) !!}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">{{trans('forms.create_user_label_role')}}</label>
                <div class="col-sm-10">
                    {!! Form::select('roles', $roles,[], array('class' => 'form-control form-control-sm')) !!}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">{{trans('forms.create_clinic_label_name')}}</label>
                <div class="col-sm-10">
                    {!! Form::select('butik', $butik,[], array('class' => 'form-control form-control-sm')) !!}
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-info btn-block">{!! trans('usersmanagement.buttons.submit') !!}</button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

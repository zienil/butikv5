@extends('layouts.app')

@section('content')
    <div class="card">
        <h5 class="card-header text-uppercase">{!! trans('titles.manageclinic') !!}
            <div class="float-right">
                <a class="btn btn-outline-warning btn-sm" href="{{ route('butiks.index') }}"> {!! trans('buttons.buttons.back') !!}</a>
            </div>
        </h5>
        <div class="card-body">
            <div id="avatar_container">
                <div class="collapseOne card-collapse collapse @if($butik->butik_image == null) show @endif">
                    <div class="card-body">
                        <div class="user-avatar text-center">
                            <i class="fas fa-h-square fa-4x"></i>
                            <p>{{ $butik->butik_code }}</p>
                        </div>
                    </div>
                </div>
                <div class="collapseTwo card-collapse collapse @if($butik->butik_image != null) show @endif">
                    <div class="card-body">
                        <div class="dz-preview"></div>
                        {!! Form::open(array('route' => ['butiks.upload', $butik->id], 'method' => 'POST', 'name' => 'avatarDropzone','id' => 'avatarDropzone', 'class' => 'form single-dropzone dropzone single', 'files' => true)) !!}
                        <img id="user_selected_avatar" class="butik-avatar"
                             src="@if ($butik->butik_image != null) {{ $butik->butik_image }} @endif"
                             alt="{{ $butik->butik_code }}">
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-10 offset-1 col-sm-10 offset-sm-1 mb-2">
                    <div class="row">
                        <div class="col-6 col-xs-6">
                            <div class="btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-outline-info @if($butik->butik_image == null) active @endif btn-block btn-sm"
                                       data-toggle="collapse" data-target=".collapseOne:not(.show), .collapseTwo.show">
                                    <input type="radio" name="avatar_status" id="option1"
                                           @if($butik->butik_image == null) checked @endif> Use Default
                                </label>
                            </div>
                        </div>
                        <div class="col-6 col-xs-6">
                            <div class="btn-group-toggle" data-toggle="buttons">
                                <label class="btn btn-outline-info @if($butik->butik_image != null) active @endif btn-block btn-sm"
                                       data-toggle="collapse" data-target=".collapseOne.show, .collapseTwo:not(.show)">
                                    <input type="radio" name="avatar_status" id="option2"
                                           @if($butik->butik_image != null) checked @endif> Use My Image
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            {!! Form::open(array('route' => 'butiks.store', 'method' => 'POST', 'role' => 'form', 'class' => 'needs-validation')) !!}
            {!! csrf_field() !!}
            <div class="form-group has-feedback row {{ $errors->has('clinicName') ? ' has-error ' : '' }}">
                {!! Form::label('clinicName', trans('forms.create_clinic_label_name'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                <div class="col-md-10">
                    <div class="input-group">
                        {!! Form::text('clinicName', $butik->butik_name, array('id' => 'clinicName', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_clinic_ph_name'))) !!}
                    </div>
                    @if ($errors->has('clinicName'))
                        <small class="form-text text-danger">
                            <strong>{{ $errors->first('clinicName') }}</strong>
                        </small>
                    @endif
                </div>
            </div>
            <div class="form-group has-feedback row {{ $errors->has('clinicAddress') ? ' has-error ' : '' }}">
                {!! Form::label('clinicAddress', trans('forms.create_clinic_label_address'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                <div class="col-md-10">
                    <div class="input-group">
                        {!! Form::text('clinicAddress', $butik->butik_address, array('id' => 'clinicAddress', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_clinic_ph_address'))) !!}
                    </div>
                    @if ($errors->has('clinicAddress'))
                        <small class="form-text text-danger">
                            <strong>{{ $errors->first('clinicAddress') }}</strong>
                        </small>
                    @endif
                </div>
            </div>
            <div class="form-group has-feedback row {{ $errors->has('clinicEmail') ? ' has-error ' : '' }}">
                {!! Form::label('clinicEmail', trans('forms.create_clinic_label_email'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                <div class="col-md-10">
                    <div class="input-group">
                        {!! Form::email('clinicEmail', $butik->butik_email, array('id' => 'clinicEmail', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_clinic_ph_email'))) !!}
                    </div>
                    @if ($errors->has('clinicEmail'))
                        <small class="form-text text-danger">
                            <strong>{{ $errors->first('clinicEmail') }}</strong>
                        </small>
                    @endif
                </div>
            </div>
            <div class="form-group has-feedback row {{ $errors->has('clinicPhone') ? ' has-error ' : '' }}">
                {!! Form::label('clinicPhone', trans('forms.create_clinic_label_phone'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                <div class="col-md-10">
                    <div class="input-group">
                        {!! Form::text('clinicPhone', $butik->butik_phone, array('id' => 'clinicPhone', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_clinic_ph_phone'))) !!}
                    </div>
                    @if ($errors->has('clinicPhone'))
                        <small class="form-text text-danger">
                            <strong>{{ $errors->first('clinicPhone') }}</strong>
                        </small>
                    @endif
                </div>
            </div>
            <div class="form-group has-feedback row {{ $errors->has('clinicPIC') ? ' has-error ' : '' }}">
                {!! Form::label('clinicPIC', trans('forms.create_clinic_label_pic'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                <div class="col-md-10">
                    <div class="input-group">
                        {!! Form::text('clinicPIC', $butik->butik_pic, array('id' => 'clinicPIC', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_clinic_ph_pic'))) !!}
                    </div>
                    @if ($errors->has('clinicPIC'))
                        <small class="form-text text-danger">
                            <strong>{{ $errors->first('clinicPIC') }}</strong>
                        </small>
                    @endif
                </div>
            </div>
            <div class="form-group has-feedback row {{ $errors->has('clinicService') ? ' has-error ' : '' }}">
                {!! Form::label('clinicService', trans('forms.create_clinic_label_service'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                <div class="col-md-10">
                    <div class="input-group">
                        {!! Form::textarea('clinicService', $butik->butik_service, array('id' => 'clinicService', 'class' => 'form-control form-control-sm','rows' => 2, 'cols' => 50, 'placeholder' => trans('forms.create_clinic_ph_service'))) !!}
                    </div>
                    @if ($errors->has('clinicService'))
                        <small class="form-text text-danger">
                            <strong>{{ $errors->first('clinicService') }}</strong>
                        </small>
                    @endif
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-info btn-block">{!! trans('buttons.buttons.submit') !!}</button>
        </div>
        {!! Form::Close() !!}
    </div>
@endsection
@section('third_party_stylesheets')

@stop
@section('third_party_scripts')
    @include('scripts.butik-avatar-dz')
@stop

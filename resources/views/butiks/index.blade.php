@extends('layouts.app')

@section('content')
    <div class="card">
        <h5 class="card-header text-uppercase"><i class='fas fa-clinic-medical mr-2'></i> {{trans('titles.butikManagement')}}
        </h5>
        <div class="card-body">
            @if(Auth::user()->can('butik-create'))
                <div class="float-right mb-2">
                    <a class="btn btn-sm btn-success"
                       href="{{ route('butiks.create') }}"> {!! trans('buttons.buttons.create-butik') !!}</a>
                </div>
            @endif
            <div class="table-responsive">
                <table id="butikList" class="table table-sm table-bordered">
                    <thead class="thead-dark text-center">
                    <tr>
                        <th scope="col" class="align-middle">#</th>
                        <th scope="col" class="align-middle">{{trans('forms.create_clinic_label_name')}}</th>
                        <th scope="col" class="align-middle">{{trans('forms.create_clinic_label_email')}}</th>
                        <th scope="col" class="align-middle">{{trans('forms.create_clinic_label_phone')}}</th>
                        <th scope="col" class="align-middle">{{trans('forms.create_clinic_label_pic')}}</th>
                        <th scope="col" class="align-middle">{{trans('forms.row_action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($butik as $key => $butiks)
                        <tr class="text-center">
                            <td class="align-middle"></td>
                            <td class="align-middle">{{ $butiks->butik_name }}</td>
                            <td class="align-middle">{{ $butiks->butik_email }}</td>
                            <td class="align-middle">{{ $butiks->butik_phone }}</td>
                            <td class="align-middle">{{ $butiks->butik_pic }}</td>
                            <td class="align-middle">
                                <a class="btn btn-sm btn-info" href="{{ route('butiks.show',$butiks->id) }}">{!! trans('usersmanagement.buttons.show') !!}</a>
                                @if(Auth::user()->can('butik-edit'))
                                    <a class="btn btn-sm btn-primary" href="{{ route('butiks.edit',$butiks->id) }}">{!! trans('usersmanagement.buttons.edit') !!}</a>
                                @endif
                                @if(Auth::user()->can('butik-delete'))
                                    {!! Form::open(array('route' => ['butiks.destroy', $butiks->id], 'title' => trans('modals.modals.delete_title'),'style'=>'display:inline')) !!}
                                    {!! Form::hidden('_method', 'DELETE') !!}
                                    {!! Form::button(trans('buttons.buttons.delete'), array('class' => 'btn btn-danger btn-sm','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => trans('modals.modals.delete_butik_title'), 'data-message' => trans('modals.modals.delete_butik_message'))) !!}
                                    {!! Form::close() !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('modals.modal-delete')
@endsection
@section('third_party_stylesheets')
    <link rel="stylesheet"
          href="{{asset('/plugins/DataTables/DataTables-1.10.25/css/dataTables.bootstrap4.min.css')}}"/>
@stop
@section('third_party_scripts')
    @include('scripts.datatables')
    @include('scripts.delete-modal-script')
@stop

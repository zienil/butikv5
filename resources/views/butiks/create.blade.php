@extends('layouts.app')

@section('content')
    <div class="card">
        <h5 class="card-header text-uppercase">{!! trans('clinic.create-new-clinic') !!}
            <div class="float-right">
                <a class="btn btn-outline-warning btn-sm" href="{{ route('butiks.index') }}"> {!! trans('buttons.buttons.back') !!}</a>
            </div>
        </h5>
        <div class="card-body">
            {!! Form::open(array('route' => 'butiks.store', 'method' => 'POST', 'role' => 'form', 'class' => 'needs-validation')) !!}
            {!! csrf_field() !!}
            <div class="form-group has-feedback row {{ $errors->has('clinicName') ? ' has-error ' : '' }}">
                {!! Form::label('clinicName', trans('forms.create_clinic_label_name'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                <div class="col-md-10">
                    <div class="input-group">
                        {!! Form::text('clinicName', null, array('id' => 'clinicName', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_clinic_ph_name'))) !!}
                    </div>
                    @if ($errors->has('clinicName'))
                        <small class="form-text text-danger">
                            <strong>{{ $errors->first('clinicName') }}</strong>
                        </small>
                    @endif
                </div>
            </div>
            <div class="form-group has-feedback row {{ $errors->has('clinicAddress') ? ' has-error ' : '' }}">
                {!! Form::label('clinicAddress', trans('forms.create_clinic_label_address'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                <div class="col-md-10">
                    <div class="input-group">
                        {!! Form::text('clinicAddress', null, array('id' => 'clinicAddress', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_clinic_ph_address'))) !!}
                    </div>
                    @if ($errors->has('clinicAddress'))
                        <small class="form-text text-danger">
                            <strong>{{ $errors->first('clinicAddress') }}</strong>
                        </small>
                    @endif
                </div>
            </div>
            <div class="form-group has-feedback row {{ $errors->has('clinicEmail') ? ' has-error ' : '' }}">
                {!! Form::label('clinicEmail', trans('forms.create_clinic_label_email'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                <div class="col-md-10">
                    <div class="input-group">
                        {!! Form::email('clinicEmail', null, array('id' => 'clinicEmail', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_clinic_ph_email'))) !!}
                    </div>
                    @if ($errors->has('clinicEmail'))
                        <small class="form-text text-danger">
                            <strong>{{ $errors->first('clinicEmail') }}</strong>
                        </small>
                    @endif
                </div>
            </div>
            <div class="form-group has-feedback row {{ $errors->has('clinicPhone') ? ' has-error ' : '' }}">
                {!! Form::label('clinicPhone', trans('forms.create_clinic_label_phone'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                <div class="col-md-10">
                    <div class="input-group">
                        {!! Form::text('clinicPhone', null, array('id' => 'clinicPhone', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_clinic_ph_phone'))) !!}
                    </div>
                    @if ($errors->has('clinicPhone'))
                        <small class="form-text text-danger">
                            <strong>{{ $errors->first('clinicPhone') }}</strong>
                        </small>
                    @endif
                </div>
            </div>
            <div class="form-group has-feedback row {{ $errors->has('clinicPIC') ? ' has-error ' : '' }}">
                {!! Form::label('clinicPIC', trans('forms.create_clinic_label_pic'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                <div class="col-md-10">
                    <div class="input-group">
                        {!! Form::text('clinicPIC', null, array('id' => 'clinicPIC', 'class' => 'form-control form-control-sm', 'placeholder' => trans('forms.create_clinic_ph_pic'))) !!}
                    </div>
                    @if ($errors->has('clinicPIC'))
                        <small class="form-text text-danger">
                            <strong>{{ $errors->first('clinicPIC') }}</strong>
                        </small>
                    @endif
                </div>
            </div>
            <div class="form-group has-feedback row {{ $errors->has('clinicService') ? ' has-error ' : '' }}">
                {!! Form::label('clinicService', trans('forms.create_clinic_label_service'), array('class' => 'col-md-2 control-label col-form-label-sm')); !!}
                <div class="col-md-10">
                    <div class="input-group">
                        {!! Form::textarea('clinicService', null, array('id' => 'clinicService', 'class' => 'form-control form-control-sm','rows' => 2, 'cols' => 50, 'placeholder' => trans('forms.create_clinic_ph_service'))) !!}
                    </div>
                    @if ($errors->has('clinicService'))
                        <small class="form-text text-danger">
                            <strong>{{ $errors->first('clinicService') }}</strong>
                        </small>
                    @endif
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-info btn-block">{!! trans('buttons.buttons.submit') !!}</button>
        </div>
        {!! Form::Close() !!}
    </div>
@endsection
@section('third_party_stylesheets')

@stop
@section('third_party_scripts')

@stop

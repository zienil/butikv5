@extends('layouts.app')


@section('content')
    <div class="card">
        <h5 class="card-header text-uppercase">{{trans('clinic.showing-details-clinic')}}
            <div class="float-right">
                <a class="btn btn-outline-warning btn-sm" href="{{ route('butiks.index') }}"> {!! trans('buttons.buttons.back') !!}</a>
            </div>
        </h5>
        <div class="card-body">
            <div class="row">
                <div class="col-4 text-center align-middle">
                    @if(!empty($butik->butik_image))
                        <img src="{{$butik->butik_image}}" alt="{{ $butik->butik_code }}"
                             class="butik-avatar-show rounded border border-dark">
                    @else
                        <i class="fas fa-h-square fa-9x"></i>
                    @endif
                </div>
                <div class="col-8">
                    <dl>
                        <dt class="col-sm-3">{{trans('forms.create_clinic_label_code')}}</dt>
                        <dd class="col-sm-9">{{ $butik->butik_code }}</dd>
                        <dt class="col-sm-3">{{trans('forms.create_clinic_label_name')}}</dt>
                        <dd class="col-sm-9">{{ $butik->butik_name }}</dd>
                        <dt class="col-sm-3">{{trans('forms.create_clinic_label_address')}}</dt>
                        <dd class="col-sm-9">{{ $butik->butik_address }}</dd>
                        <dt class="col-sm-3">{{trans('forms.create_clinic_label_email')}}</dt>
                        <dd class="col-sm-9">{{ $butik->butik_email }}</dd>
                        <dt class="col-sm-3">{{trans('forms.create_clinic_label_phone')}}</dt>
                        <dd class="col-sm-9">{{ $butik->butik_phone }}</dd>
                        <dt class="col-sm-3">{{trans('forms.create_clinic_label_pic')}}</dt>
                        <dd class="col-sm-9">{{ $butik->butik_pic }}</dd>
                        <dt class="col-sm-3">{{trans('forms.create_clinic_label_service')}}</dt>
                        <dd class="col-sm-9">{{ $butik->butik_service }}</dd>
                        {{--                <dt class="col-sm-3">{{trans('forms.permission')}}</dt>--}}
                        {{--                <dd class="col-sm-9">--}}
                        {{--                    @if(!empty($rolePermissions))--}}
                        {{--                        <ul class="list-inline">--}}
                        {{--                            @foreach($rolePermissions as $v)--}}
                        {{--                                <li class="list-inline-item"><span class="badge badge-primary">{{ $v->name }}</span>--}}
                        {{--                                </li>--}}
                        {{--                            @endforeach--}}
                        {{--                        </ul>--}}
                        {{--                    @else--}}
                        {{--                        <span class="badge badge-secondary">{{trans('forms.noPermission')}}</span>--}}
                        {{--                    @endif--}}
                        {{--                </dd>--}}
                    </dl>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="card border-dark">
                        <div class="card-header">
                            Perunding
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="tablePerunding" class="table table-sm table-bordered">
                                    <thead class="thead-dark text-center">
                                    <tr>
                                        <th scope="col" class="align-middle">#</th>
                                        <th scope="col" class="align-middle">{{trans('forms.create_user_label_fullname')}}</th>
                                        <th scope="col" class="align-middle">{{trans('forms.create_user_label_email')}}</th>
                                        <th scope="col" class="align-middle">{{trans('forms.create_user_label_phone')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users AS $list)
                                        @if($list->roleid == 4)
                                            <tr class="text-center">
                                                <td class="align-middle"></td>
                                                <td class="align-middle">{{ $list->fullname }}</td>
                                                <td class="align-middle">{{ $list->email }}</td>
                                                <td class="align-middle">{{ $list->phone }}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card border-info">
                        <div class="card-header">
                            Juruterapi
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table id="tableJuruterapi" class="table table-sm table-bordered">
                                    <thead class="thead-dark text-center">
                                    <tr>
                                        <th scope="col" class="align-middle">#</th>
                                        <th scope="col" class="align-middle">{{trans('forms.create_user_label_fullname')}}</th>
                                        <th scope="col" class="align-middle">{{trans('forms.create_user_label_email')}}</th>
                                        <th scope="col" class="align-middle">{{trans('forms.create_user_label_phone')}}</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($users AS $list)
                                        @if($list->roleid == 6)
                                            <tr class="text-center">
                                                <td class="align-middle"></td>
                                                <td class="align-middle">{{ $list->fullname }}</td>
                                                <td class="align-middle">{{ $list->email }}</td>
                                                <td class="align-middle">{{ $list->phone }}</td>
                                            </tr>
                                        @endif
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('third_party_stylesheets')
    <link rel="stylesheet" href="{{asset('/plugins/DataTables/DataTables-1.10.25/css/dataTables.bootstrap4.min.css')}}"/>
@stop
@section('third_party_scripts')
    @include('scripts.datatables')
@stop

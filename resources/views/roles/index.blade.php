@extends('layouts.app')

@section('content')
    <div class="card">
        <h5 class="card-header text-uppercase"><i class='fas fa-shield-alt mr-2'></i> {{trans('titles.roleManagement')}}</h5>
        <div class="card-body">
            @if(Auth::user()->can('role-create'))
                <div class="float-right mb-2">
                    <a class="btn btn-sm btn-success" href="{{ route('roles.create') }}"> {!! trans('buttons.buttons.newRole') !!}</a>
                </div>
            @endif
            <div class="table-responsive">
                <table id="rolesList" class="table table-sm table-bordered">
                    <thead class="thead-dark text-center">
                    <tr>
                        <th scope="col" class="align-middle">No</th>
                        <th scope="col" class="align-middle">{{trans('forms.roleName')}}</th>
                        <th scope="col" class="align-middle">{{trans('forms.row_action')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach ($roles as $key => $role)
                        <tr class="text-center">
                            <td class="align-middle"></td>
                            <td class="align-middle">{{ $role->name }}</td>
                            <td class="align-middle">
                                <a class="btn btn-sm btn-info" href="{{ route('roles.show',$role->id) }}">{!! trans('buttons.buttons.show') !!}</a>
                                @if(Auth::user()->can('role-edit'))
                                    <a class="btn btn-sm btn-primary" href="{{ route('roles.edit',$role->id) }}">{!! trans('buttons.buttons.edit') !!}</a>
                                @endif
                                @if(Auth::user()->can('role-delete'))
                                    {!! Form::open(array('route' => ['roles.destroy', $role->id], 'title' => trans('modals.modals.delete_title'),'style'=>'display:inline')) !!}
                                    {!! Form::hidden('_method', 'DELETE') !!}
                                    {!! Form::button(trans('buttons.buttons.delete'), array('class' => 'btn btn-danger btn-sm','type' => 'button', 'data-toggle' => 'modal', 'data-target' => '#confirmDelete', 'data-title' => trans('modals.modals.delete_role_title'), 'data-message' => trans('modals.modals.delete_role_message'))) !!}
                                    {!! Form::close() !!}
                                @endif
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    @include('modals.modal-delete')
@endsection
@section('third_party_stylesheets')
    <link rel="stylesheet"
          href="{{asset('/plugins/DataTables/DataTables-1.10.25/css/dataTables.bootstrap4.min.css')}}"/>
@stop
@section('third_party_scripts')
    @include('scripts.datatables')
    @include('scripts.delete-modal-script')
@stop

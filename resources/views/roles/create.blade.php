@extends('layouts.app')

@section('content')
    <div class="card">
        <h5 class="card-header text-uppercase">{{trans('titles.createNewRole')}}
            <div class="float-right">
                <a class="btn btn-outline-warning btn-sm" href="{{ route('roles.index') }}"> {!! trans('buttons.buttons.back') !!}</a>
            </div>
        </h5>
        {!! Form::open(array('route' => 'roles.store','method'=>'POST')) !!}
        <div class="card-body">
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">{{trans('forms.create_user_label_rolename')}}</label>
                <div class="col-sm-10">
                    {!! Form::text('name', null, array('placeholder' => trans('forms.create_user_ph_rolename'),'class' => 'form-control form-control-sm')) !!}
                </div>
            </div>
            <div class="form-group row">
                <label class="col-sm-2 col-form-label">{{trans('forms.create_user_label_permission')}}</label>
                <div class="col-sm-10">
                    @foreach($permission as $value)
                        <div class="form-check">
                            {{ Form::checkbox('permission[]', $value->id, false, array('class' => 'form-check-input')) }}
                            <label class="form-check-label">{{ $value->name }}</label>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="card-footer">
            <button type="submit" class="btn btn-sm btn-info btn-block">{!! trans('buttons.buttons.submit') !!}</button>
        </div>
        {!! Form::close() !!}
    </div>
@endsection

@extends('layouts.app')


@section('content')
    <div class="card">
        <h5 class="card-header text-uppercase">{{trans('titles.roleShow')}}
            <div class="float-right">
                <a class="btn btn-outline-warning btn-sm" href="{{ route('roles.index') }}"> {!! trans('buttons.buttons.back') !!}</a>
            </div>
        </h5>
        <div class="card-body">
            <dl class="row">
                <dt class="col-sm-3">{{trans('forms.roleName')}}</dt>
                <dd class="col-sm-9">{{ $role->name }}</dd>
                <dt class="col-sm-3">{{trans('forms.permission')}}</dt>
                <dd class="col-sm-9">
                    @if(!empty($rolePermissions))
                        <ul class="list-inline">
                            @foreach($rolePermissions as $v)
                                <li class="list-inline-item"><span class="badge badge-primary">{{ $v->name }}</span>
                                </li>
                            @endforeach
                        </ul>
                    @else
                        <span class="badge badge-secondary">{{trans('forms.noPermission')}}</span>
                    @endif
                </dd>
            </dl>
        </div>
    </div>
@endsection

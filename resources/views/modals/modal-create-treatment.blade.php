<div class="modal fade modal-add modal-save" id="createTreatment" data-backdrop="static" role="dialog" aria-labelledby="createTreatmentLabel" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    {!! trans('modals.create_treatment_modal_button_save_text') !!}
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">close</span>
                </button>
            </div>
            {{--START FORM--}}
            {!! Form::open(array('route' => 'treatments.store', 'method' => 'POST', 'class' => 'needs-validation','id'=>'addTreatmentForm')) !!}
            {!! csrf_field() !!}
            <div class="modal-body">
                <input type="hidden" name="action" id="action" value="Treatment">
                <input type="hidden" name="patientID" id="patientID" value="{{$patient->id}}">
                <div class="form-group has-feedback row {{ $errors->has('treatment_type') ? ' has-error ' : '' }}">
                    {!! Form::label('treatment_type', trans('forms.create_user_label_treatment_type'), array('class' => 'col-md-4 control-label col-form-label-sm font-weight-bold')); !!}
                    <div class="col-md-5">
                        <select class="form-control form-control-sm" name="treatment_type" id="treatment_type" required>
                            <option value="">{{ trans('forms.create_user_ph_treatment_type') }}</option>
                            <option value="Baru">Baru</option>
                            <option value="Susulan">Susulan</option>
                            <option value="Acute">Acute</option>
                            <option value="Lain-Lain">Lain-Lain</option>
                        </select>
                        @if ($errors->has('treatment_type'))
                            <span class="help-block">
                                <strong>{{ $errors->first('treatment_type') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
                <div class="form-group has-feedback row {{ $errors->has('others') ? ' has-error ' : '' }} isOthers" style="display: none;">
                    <div class="col-md-6 offset-md-4">
                        {!! Form::text('others', null, array('id' => 'others', 'class' => 'form-control form-control-sm','autofocus', 'placeholder' => trans('forms.create_user_ph_treatment_type_Others'))) !!}
                        @if ($errors->has('others'))
                            <span class="help-block">
                                <strong>{{ $errors->first('others') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                {!! Form::button('<i class="fa fa-fw '.trans('modals.confirm_modal_button_cancel_icon').'" aria-hidden="true"></i> ' . trans('modals.confirm_modal_button_cancel_text'), array('class' => 'btn btn-outline pull-left btn-flat', 'type' => 'button', 'data-dismiss' => 'modal' )) !!}
                {!! Form::button('<i class="fa fa-fw '.trans('modals.confirm_modal_button_save_icon').'" aria-hidden="true"></i> ' . trans('modals.confirm_modal_button_save_text'), array('class' => 'btn btn-success btn-sm pull-right btn-flat hvr-glow', 'type' => 'submit', 'id' => 'confirmTreatmentAdd' )) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>

<div class="modal fade modal-danger" id="confirmDelete" role="dialog" aria-labelledby="confirmDeleteLabel" aria-hidden="true" tabindex="-1">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">
                    Confirm Delete
                </h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">close</span>
                </button>
            </div>
            <div class="modal-body">
                <p>Delete this data?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-sm btn-outline pull-left btn-light" data-dismiss="modal"><i class="fas fa-times"></i>{!! trans('modals.modals.cancel_btn') !!}</button>
                <button type="button" class="btn btn-sm btn-danger pull-right" id="confirm"><i class="fas fa-trash-alt"></i>{!! trans('modals.modals.delete_btn') !!}</button>
            </div>
        </div>
    </div>
</div>

<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

    {{-- CSRF Token --}}
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@hasSection('template_title')@yield('template_title') | @endif {{ config('app.name', Lang::get('titles.app')) }}</title>
    <meta name="description" content="Butikv3@Sihat2U">
    <meta name="author" content="theZie">
    <link rel="shortcut icon" href="/favicon.ico">

    {{-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries --}}
        <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    {{-- Fonts --}}
    @yield('template_linked_fonts')

    {{-- Styles --}}
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet">
    <link href="/css/coreui-icons/css/all.css" rel="stylesheet">

    @yield('third_party_stylesheets')
    {{-- Scripts --}}
    <script>
        window.Laravel = {!! json_encode(['csrfToken' => csrf_token(),]) !!};
    </script>

    @yield('head')
</head>
<body class="c-app">
{{--SIDEBAR--}}
<div class="c-sidebar c-sidebar-dark c-sidebar-fixed c-sidebar-lg-show" id="sidebar">
    @include('partials.sidebar')
</div>
<div class="c-wrapper c-fixed-components">
    {{--    NAVBAR--}}
    <header class="c-header c-header-light c-header-fixed c-header-with-subheader">
        @include('partials.navbar')
    </header>
    <div class="c-body">
        <main class="c-main">
            <div class="container-fluid">
                <div class="fade-in">
                    <div class="row">
                        <div class="col-12">
                            @include('partials.form-status')
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            @yield('content')
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <footer class="c-footer">
            @include('partials.footer')
        </footer>
    </div>
</div>

{{-- Scripts --}}
<script src="{{ mix('/js/app.js') }}"></script>
@yield('third_party_scripts')
@stack('page_scripts')
</body>
</html>

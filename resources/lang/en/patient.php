<?php

return [
    'showing-all-patients'    => 'Showing All Patients',
    'showing-details-patient' => 'Showing Details Patient',
    'patient-menu-alt'        => 'Show Patient Management Menu',
    'create-new-patient'      => 'Create New Patient',
    'pre-reg-patient'         => 'Pre-Registration Patient',
    'search-all-patients'     => 'Searching Patients',


    // Flash Messages
    'createSuccess'           => 'Successfully created New Patient ',
    'updateSuccess'           => 'Successfully updated patient data ',
    'deleteSuccess'           => 'Successfully deleted patient data ',
    'deleteSelfError'         => 'You cannot delete yourself! ',

    'buttons' => [
        'create-new'             => 'New Patient',
        'delete'                 => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Delete</span><span class="hidden-xs hidden-sm hidden-md"></span>',
        'show'                   => '<i class="fas fa-info-circle"></i> <span class="hidden-xs hidden-sm">Show</span><span class="hidden-xs hidden-sm hidden-md"></span>',
        'edit'                   => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Edit</span><span class="hidden-xs hidden-sm hidden-md"></span>',
        'treatment'              => '<i class="fa fa-medkit fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Treatment</span><span class="hidden-xs hidden-sm hidden-md"></span>',
        'back-to-clinics'        => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs">Butik@Sihat2U</span>',
        'back-to-clinic'         => 'Back  <span class="hidden-xs">to Butik@Sihat2U</span>',
        'delete-user'            => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Delete</span><span class="hidden-xs"> Patient</span>',
        'edit-user'              => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Edit</span><span class="hidden-xs"> Patient</span>',
        'treatment-patient'      => '<i class="fa fa-stethoscope fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Treatment</span><span class="hidden-xs"></span>',
        'treatment-edit-patient' => '<i class="fa fa-stethoscope fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Edit Treatment</span><span class="hidden-xs"></span>',
    ],

    'tooltips' => [
        'delete'        => 'Delete',
        'show'          => 'Show',
        'edit'          => 'Edit',
        'create-new'    => 'Create New Patient',
        'back-patients' => 'Back to Patient\'s',
        'email-user'    => 'Email :user',
        'submit-search' => 'Submit Users Search',
        'clear-search'  => 'Clear Search Results',
    ],
];

<?php

return [
	'showing-all-clinic'     => 'Showing All Butik',
	'showing-details-clinic' => 'Showing Details Butik',
	'clinic-menu-alt'        => 'Show Butik Management Menu',
	'create-new-clinic'      => 'Create New Butik',
    'create-new-clinic'      => 'Create New Butik',


	// Flash Messages
	'createSuccess'          => 'Successfully created Butik! ',
	'updateSuccess'          => 'Successfully updated Butik! ',
	'deleteSuccess'          => 'Successfully deleted Butik! ',
	'deleteSelfError'        => 'You cannot delete yourself! ',

	'buttons' => [
		'create-new'      => 'New Butik',
		'delete'          => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Delete</span><span class="hidden-xs hidden-sm hidden-md"></span>',
		'show'            => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Show</span><span class="hidden-xs hidden-sm hidden-md"></span>',
		'edit'            => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Edit</span><span class="hidden-xs hidden-sm hidden-md"></span>',
		'back-to-clinics' => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs">Butik</span>',
		'back-to-clinic'  => 'Back  <span class="hidden-xs">to Butik</span>',
		'delete-user'     => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Delete</span><span class="hidden-xs"> Butik</span>',
		'edit-user'       => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Edit</span><span class="hidden-xs"> Butik</span>',
	],

	'tooltips' => [
		'delete'        => 'Delete',
		'show'          => 'Show',
		'edit'          => 'Edit',
		'create-new'    => 'Create New Butik',
		'back-clinics'  => 'Back to Butik\'s',
		'email-user'    => 'Email :user',
		'submit-search' => 'Submit Users Search',
		'clear-search'  => 'Clear Search Results',
	],
];

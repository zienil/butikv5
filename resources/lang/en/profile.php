<?php

return [

    /*
    |--------------------------------------------------------------------------
    | User Profiles Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match user profile
    | pages, messages, forms, labels, placeholders, links, and buttons.
    |
    */

    'templateTitle'               => 'Edit your profile',
    'userProfile'                 => 'User Profile',
    'showProfile'                 => 'Show Profile',

    // profile errors
    'notYourProfile'              => 'This isn\'t the profile you\'re looking for.',
    'notYourProfileTitle'         => 'Hmmm, something went wrong...',
    'noProfileYet'                => 'No profile yet.',

    // USER profile title

    // USER profile page
    'showProfileTitle'            => 'PROFILE',
    'showProfileMrn'              => 'Registration No',
    'showProfileFullName'         => 'Full Name',
    'showProfileUsername'         => 'Username',
    'showProfilemykadProfile'     => 'myKad/Passport',
    'showProfilePhone'            => 'Phone',
    'showProfileEmail'            => 'Email',
    'showProfilePrivilege'        => 'Privilege',
    'showAddressProfile'          => 'Full Address',
    'showPostcodeProfile'         => 'Postcode',
    'showStateProfile'            => 'State',
    'showCountryProfile'          => 'Country',
    'showAgeProfile'              => 'Age',
    'showGenderProfile'           => 'Gender',
    'showRaceProfile'             => 'Race',
    'showReligionProfile'         => 'Religion',
    'showMaritalProfile'          => 'Marital',
    'showCitizenshipProfile'      => 'Citizenship',
    'showNationalityProfile'      => 'Nationality',
    'showOccupationProfile'       => 'Occupation',
    'showProfileButiks'           => 'Butik Name',
    'showProfileRegistrationDate' => 'Date Register',

    // USER EDIT profile page
    'editProfileTitle'            => 'Profile Settings',

    // User Account Settings Tab
    'editTriggerAlt'              => 'Toggle User Menu',
    'editAccountTitle'            => 'Account Settings',
    'editAccountAdminTitle'       => 'Account Administration',
    'updateAccountSuccess'        => 'Your account has been successfully updated',
    'submitProfileButton'         => 'Save Changes',

    // User Account Admin Tab
    'submitPWButton'              => 'Update Password',
    'changePwTitle'               => 'Change Password',
    'changePwPill'                => 'Change Password',
    'deleteAccountPill'           => 'Delete Account',
    'updatePWSuccess'             => 'Your password has been successfully updated',

    // Delete Account Tab
    'deleteAccountTitle'          => 'Delete Account',
    'deleteAccountBtn'            => 'Delete My Account',
    'deleteAccountBtnConfirm'     => 'Delete My Account',
    'deleteAccountConfirmTitle'   => 'Confirm Account Deletion',
    'deleteAccountConfirmMsg'     => 'Are you sure you want to delete your account?',
    'confirmDeleteRequired'       => 'Confirm Account Deletion is required',

    'errorDeleteNotYour'        => 'You can only delete your own profile',
    'successUserAccountDeleted' => 'Your account has been deleted',

    // Messages
    'updateSuccess'             => 'Your profile has been successfully updated',
    'submitButton'              => 'Save Changes',

    // Restore User Account
    'errorRestoreUserTime'      => 'Sorry, account cannot be restored',
    'successUserRestore'        => 'Welcome back :username! Account Successfully Restored',

];

<?php

return [
    //BUTTON
    'buttons' => [
        'delete'                 => '<i class="fas fa-trash-alt mr-2"></i>Delete',
        'show'                   => '<i class="fas fa-info-circle mr-2"></i>Show',
        'edit'                   => '<i class="fas fa-user-edit mr-2"></i>Edit',
        'back'                   => '<i class="fas fa-arrow-circle-left mr-1"></i>Back',
        'submit'                 => '<i class="fas fa-paper-plane mr-1"></i> Submit',
        'update'                 => '<i class="fas fa-paper-plane mr-1"></i> Update',
        'newUser'                => '<i class="fas fa-user-plus mr-2"></i>Add New User',
        'newRole'                => '<i class="fas fa-shield-alt mr-2"></i>Add New Role',
        'create-butik'           => '<i class="fas fa-clinic-medical mr-2"></i>Add Butik',
        'create-new'             => 'New Patient',
        'edit-profile'           => '<i class="fas fa-cog mr-2"></i>Edit Profile',
        'treatment'              => '<i class="fa fa-medkit fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Treatment</span><span class="hidden-xs hidden-sm hidden-md"></span>',
        'back-to-clinics'        => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs">Butik@Sihat2U</span>',
        'back-to-clinic'         => 'Back  <span class="hidden-xs">to Butik@Sihat2U</span>',
        'delete-user'            => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Delete</span><span class="hidden-xs"> Patient</span>',
        'edit-user'              => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Edit</span><span class="hidden-xs"> Patient</span>',
        'treatment-patient'      => '<i class="fa fa-stethoscope fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Treatment</span><span class="hidden-xs"></span>',
        'treatment-edit-patient' => '<i class="fa fa-stethoscope fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Edit Treatment</span><span class="hidden-xs"></span>',
    ],
]
?>

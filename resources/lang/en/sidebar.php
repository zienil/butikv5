<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'language'          => 'Languages',
    'adminDropdownNav'  => "Admin",
    'adminUsers'        => "Users",
    'usersList'         => "Users List",
    'usersRole'         => "Roles",
    'butik'             => "Butik's",
    'butik-list'        => "Lists Butik",
    'patients'          => "Patients",
    'patients-register' => "Registration",
    'patients-search'   => "Search",

];

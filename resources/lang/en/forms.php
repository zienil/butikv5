<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Forms Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match forms.
    |
    */

    // CREATE NEW USER FORM
    'create_user_label_email' => 'User Email',
    'create_user_ph_email'    => 'User Email',
    'create_user_icon_email'  => 'fa-envelope',

    'create_user_label_phone' => 'Phone',
    'create_user_ph_phone'    => 'Phone',

    'create_user_label_username' => 'Username',
    'create_user_ph_username'    => 'Username',
    'create_user_icon_username'  => 'fa-user',

    'create_user_label_fullname' => 'Full Name',
    'create_user_ph_fullname'    => 'Full Name',

    'create_user_label_firstname' => 'First Name',
    'create_user_ph_firstname'    => 'First Name',
    'create_user_icon_firstname'  => 'fa-user',

    'create_user_label_lastname' => 'Last Name',
    'create_user_ph_lastname'    => 'Last Name',
    'create_user_icon_lastname'  => 'fa-user',

    'create_user_label_icnumber' => 'IC Number',
    'create_user_ph_icnumber'    => 'IC Number without (-)',
    'create_user_icon_icnumber'  => 'fa-id-badge',

    'create_user_label_password' => 'Password',
    'create_user_ph_password'    => 'Password',
    'create_user_icon_password'  => 'fa-lock',

    'create_user_label_pw_confirmation' => 'Confirm Password',
    'create_user_ph_pw_confirmation'    => 'Confirm Password',
    'create_user_icon_pw_confirmation'  => 'fa-lock',

    'create_user_label_bio' => 'User Bio',
    'create_user_ph_bio'    => 'User Bio',
    'create_user_icon_bio'  => 'fa-pencil',

    'create_user_label_career_title' => 'User Occupation',
    'create_user_ph_career_title'    => 'User Occupation',
    'create_user_icon_career_title'  => 'fa-briefcase',

    'create_user_label_education' => 'User Education',
    'create_user_ph_education'    => 'User Education',
    'create_user_icon_education'  => 'fa-graduation-cap',

    'create_user_label_role' => 'User Role',
    'create_user_ph_role'    => 'Select User Role',
    'create_user_icon_role'  => 'fa fa-fw fa-shield',

    'create_clinic_label_role' => 'User Butik',
    'create_clinic_ph_role'    => 'Select User Butik',
    'create_clinic_icon_role'  => 'fa fa-fw fa-hospital-o',

    'create_user_button_text' => 'Create New User',

    // EDIT USER AS ADMINISTRATOR FORM
    'edit-user-admin-title'   => 'Edit User Information',

    'label-username' => 'Username',
    'ph-username'    => 'Username',

    'label-useremail' => 'User Email',
    'ph-useremail'    => 'User Email',

    'label-userrole_id' => 'User Access Level',
    'option-label'      => 'Select a Level',
    'option-user'       => 'User',
    'option-editor'     => 'Editor',
    'option-admin'      => 'Administrator',
    'submit-btn-text'   => 'Edit the User!',

    'submit-btn-icon' => 'fa-save',
    'username-icon'   => 'fa-user',
    'useremail-icon'  => 'fa-envelope-o',

    'change-pw'                        => 'Change Password',
    'cancel'                           => 'Cancel',
    'save-changes'                     => '<i class="fa fa-fw fa-save" aria-hidden="true"></i> Save Changes',

    // Search Users Form
    'search-users-ph'                  => 'Search Users',

    //ROLES
    'roleName'                         => 'Role Name',
    'permission'                       => 'Permission',
    'noPermission'                     => 'No Permission',
    'create_user_label_rolename'       => 'Role Name',
    'create_user_ph_rolename'          => 'Role Name',
    'create_user_label_permission'     => 'Permission Type',

    // Clinic
    'create_clinic_label_code'         => 'Butik Code',
    'create_clinic_label_name'         => 'Butik Name',
    'create_clinic_ph_name'            => 'Butik Name',
    'create_clinic_label_address'      => 'Butik Address',
    'create_clinic_ph_address'         => 'Butik Address',
    'create_clinic_label_location'     => 'Butik Location',
    'create_clinic_ph_location'        => 'Butik Location',
    'create_clinic_label_email'        => 'Butik Email',
    'create_clinic_ph_email'           => 'Butik Email',
    'create_clinic_label_phone'        => 'Butik Phone',
    'create_clinic_ph_phone'           => 'Butik Phone',
    'create_clinic_label_pic'          => 'Person in Charge',
    'create_clinic_ph_pic'             => 'Person in Charge',
    'create_clinic_label_service'      => 'Butik Services',
    'create_clinic_ph_service'         => 'Butik Services',
    'create_clinic_button_text'        => 'Add Butik',

    // Patient
    'create_patient_label_mrn'         => 'MRN',
    'create_patient_label_name'        => 'Full Name',
    'create_patient_ph_name'           => 'Full Name',
    'create_patient_label_ic'          => 'Malaysia NRIC/Passport',
    'create_patient_ph_ic'             => 'Malaysia NRIC (with \'-\' )',
    'create_patient_label_phone'       => 'Phone Number',
    'create_patient_ph_phone'          => 'Phone Number (without \'-\' )',
    'create_patient_label_email'       => 'Email',
    'create_patient_ph_email'          => 'Email',
    'create_patient_label_age'         => 'Age',
    'create_patient_ph_age'            => 'Age',
    'create_patient_label_gender'      => 'Gender',
    'create_patient_ph_gender'         => 'Gender',
    'create_patient_label_citizen'     => 'Citizenship',
    'create_patient_ph_citizen'        => 'Citizenship',
    'create_patient_label_nationality' => 'Nationality',
    'create_patient_ph_nationality'    => 'Nationality',
    'create_patient_label_race'        => 'Race',
    'create_patient_ph_race'           => 'Race',
    'create_patient_label_occupation'  => 'Occupation',
    'create_patient_ph_occupation'     => 'Occupation',
    'create_patient_label_address'     => 'Address',
    'create_patient_ph_address'        => 'Address',
    'create_patient_label_city'        => 'City',
    'create_patient_ph_city'           => 'City',
    'create_patient_label_state'       => 'State',
    'create_patient_ph_state'          => 'State',
    'create_patient_label_postal'      => 'Postcode',
    'create_patient_ph_postal'         => 'Postcode',

    'create_patient_button_text'                 => '<i class="fas fa-user-plus mr-2"></i>Add Patient',
    'update_patient_button_text'                 => 'Save Changes',
    'search_patient_button_text'                 => 'Search',
    'search_clear_patient_button_text'           => 'Clear Search',

    //TREATMENT
    'create_user_label_treatment_type'           => 'Type of Treatment',
    'create_user_ph_treatment_type'              => '-- Type of Treatment --',
    'create_user_label_treatment_type_Others'    => 'Others',
    'create_user_ph_treatment_type_Others'       => 'Others',
    'create_user_label_current_diagnos'          => 'Current Diagnosis',
    'create_user_ph_current_diagnos'             => 'Current Diagnosis',
    'create_user_label_health_history'           => 'Health History',
    'collapse_label_current_diagnos'             => 'Current Diagnosis',
    'collapse_label_health_history'              => 'Health History',
    'collapse_label_treatment_history'           => 'History of Treatment',
    'collapse_label_vital_point'                 => 'Vital Signs',
    'collapse_label_analysis_justification'      => 'Analysis Justification',
    'collapse_label_observation_treatment'       => 'Observation of Treatment',
    'collapse_label_follow_up_treatment'         => 'Follow Up Treatment',
    'collapse_table_head_treatment_type'         => 'Accepted Treatment Types',
    'collapse_table_head_treatment_type_specify' => 'Specify the Type of Treatment / Treatment / Therapy',
    'create_user_label_blood_pressure'           => 'Blood Pressure [mmHg]',
    'create_user_ph_blood_pressure'              => 'Blood Pressure [mmHg]',
    'create_user_label_weight'                   => 'Weight [kg]',
    'create_user_ph_weight'                      => 'Weight [kg]',
    'create_user_label_high'                     => 'High [cm]',
    'create_user_ph_high'                        => 'High [cm]',
    'create_user_label_pulse'                    => 'Pulse [/Minute]',
    'create_user_ph_pulse'                       => 'Pulse [/Minute]',
    'create_user_label_body_temperature'         => 'Body Temperature [\'&#8451;\']',
    'create_user_ph_body_temperature'            => 'Body Temperature [\'&#8451;\']',
    'create_user_label_glucose'                  => 'Glucose [mmol/L]',
    'create_user_ph_glucose'                     => 'Glucose [mmol/L]',
    'create_user_label_pain'                     => 'Scale of Pain',
    'create_user_ph_pain'                        => 'Scale of Pain',
    'create_user_label_das'                      => 'D.A.S',
    'create_user_ph_das'                         => 'D.A.S',
    'create_user_label_tls1'                     => 'T.L.S 1',
    'create_user_ph_tls1'                        => 'T.L.S 1',
    'create_user_label_tls2'                     => 'T.L.S 2',
    'create_user_ph_tls2'                        => 'T.L.S 2',
    'create_user_label_tls3'                     => 'T.L.S 3',
    'create_user_ph_tls3'                        => 'T.L.S 3',
    'create_user_label_ps'                       => 'P.S',
    'create_user_ph_ps'                          => 'P.S',
    'create_user_label_analysis_notes'           => 'Analysis Notes',
    'create_user_ph_analysis_notes'              => 'Analysis Notes',
    'create_user_label_tkms'                     => 'T.K.M.S',
    'create_user_ph_tkms'                        => 'T.K.M.S',
    'create_user_label_pkm'                      => 'P.K.M',
    'create_user_ph_pkm'                         => 'P.K.M',
    'create_user_label_justification'            => 'Justification',
    'create_user_ph_justification'               => 'Justification',
    'create_user_label_contra_indication'        => 'Contra Indication',
    'create_user_ph_contra_indication'           => 'Contra Indication',
    'create_user_label_tap'                      => 'Therapeutic Application Proposal',
    'create_user_ph_tap'                         => 'Therapeutic Application Proposal',
    'create_user_label_therapy_type'             => 'Types of Therapy',
    'create_user_ph_therapy_type'                => '-- Types of Therapy --',
    'create_user_label_odt'                      => 'Observation During Therapy',
    'create_user_ph_odt'                         => 'Observation During Therapy',
    'create_user_label_rtop'                     => 'References to Other Practitioners',
    'create_user_ph_rtop'                        => 'References to Other Practitioners',
    'create_user_label_ftp'                      => 'Follow-up Therapy Plan',
    'create_user_ph_ftp'                         => 'Follow-up Therapy Plan',
    'create_user_label_n3d'                      => 'Next 3 days',
    'create_user_ph_n3d'                         => 'Next 3 days',
    'create_user_label_n7d'                      => 'Next 7 days',
    'create_user_ph_n7d'                         => 'Next 7 days',
    'create_user_label_n14d'                     => 'Next 14 days',
    'create_user_ph_n14d'                        => 'Next 14 days',

    'create_treatment_button_text' => 'Save Treatment',
    'create_currdiag_button_text'  => 'Save Current Diagoses',
    'create_hh_button_text'        => 'Save Health History',

    'row_action'   => 'Actions',
    'span_none'    => 'None',
    'male'         => 'Male',
    'female'       => 'Female',
    'citizen'      => 'Citizen',
    'nonCitizen'   => 'Non-Citizen',
    'choose'       => '-- Choose --',
    'dateRegister' => 'Date Register',
    'noData'       => 'No Data Found!!',
    'search-info'  => 'Searching Data',

];

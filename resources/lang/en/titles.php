<?php

return [

    'app'              => 'Sihat2U',
    'app2'             => 'Butik v:version',
    'home'             => 'Home',
    'login'            => 'Login',
    'logout'           => 'Logout',
    'register'         => 'Register',
    'resetPword'       => 'Reset Password',
    'toggleNav'        => 'Toggle Navigation',
    'profile'          => 'Profile',
    'editProfile'      => 'Edit Profile',
    'createProfile'    => 'Create Profile',
    'adminDropdownNav' => 'Admin',
    'mainPage'         => 'Home',

    'activation' => 'Registration Started  | Activation Required',
    'exceeded'   => 'Activation Error !!!',

    'adminUserList'  => 'Users Data',
    'adminEditUsers' => 'Edit Users',
    'adminNewUser'   => 'Create New User',

    'adminThemesList' => 'Themes',
    'adminThemesAdd'  => 'Add New Theme',

    'adminLogs'     => 'Log Files',
    'adminActivity' => 'Activity Log',
    'adminPHP'      => 'PHP Information',
    'adminRoutes'   => 'Routing Details',

    'activeUsers'    => 'Active Users',
    'laravelBlocker' => 'Blocker',

    'laravelroles'     => 'User Roles',
    'manageusers'      => 'Manage User',

    //ROLE
    'roleEdit'         => "Edit Role",
    'roleManagement'   => 'Roles Management',
    'createNewRole'    => "Create New Role",
    'roleShow'         => "Role Details",

    //PENGAMAL
    'pengamalNav'      => 'Pengamal',
    'addpengamal'      => 'Register Pengamal',

    //PATIENTS
    'patientNav'       => 'Patients',
    'patientList'      => 'Patients Data',
    'patientSearch'    => 'Patients Search',
    'managepatient'    => 'Manage Patients',
    'addpatient'       => 'Add Patient',
    'preReg'           => 'Pre Registration',

    //	CLINICS
    'clinicNav'        => 'Butik',
    'butikManagement'  => 'Butik Management',
    'manageclinic'     => 'Manage Butik',
    'createClinic'     => 'Create Butik',
    'clinicList'       => 'Lists Butik',
    'clinicReg'        => 'Register Butik',

    // TREATMENT
    'patientTreatment' => 'Treatment',
];

<?php

return [
    //LOGIN PAGE
    'login'            => "Login",
    'register'         => "Register",
    'username_email'   => "Username or Email",
    'password'         => "Password",
    'rememberme'       => "Remember Me",
    'login_btn'        => "Login",
    'forgot_pass_btn'  => "Forgot Your Password?",

    //REGISTER PAGE
    'register_title'   => "Register",
    'full_name'        => "Full Name",
    'username'         => "Username",
    'email_add'        => "Email Address",
    'password_1'       => "Password",
    'password_2'       => "Confirm Password",
    'register_btn'     => "Register",

    //SIDEBAR

]
?>

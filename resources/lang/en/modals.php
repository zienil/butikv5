<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Modals Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which modals share.
    |
    */

    // Default Save Modal;
    'confirm_modal_title_text'                  => 'Confirm Save',
    'confirm_modal_title_std_msg'               => 'Please confirm your request.',

    // Confirm Save Modal;
    'confirm_modal_button_save_text'            => 'Save Changes',
    'confirm_modal_button_save_icon'            => 'fa-save',
    'confirm_modal_button_cancel_text'          => 'Cancel',
    'confirm_modal_button_cancel_icon'          => 'fa-times',
    'edit_user__modal_text_confirm_title'       => 'Confirm Save',
    'edit_user__modal_text_confirm_message'     => 'Please confirm your changes.',

    // Form Modal
    'form_modal_default_title'                  => 'Confirm',
    'form_modal_default_message'                => 'Please Confirm',
    'form_modal_default_btn_cancel'             => 'Cancel',
    'form_modal_default_btn_submit'             => 'Confirm Submit',


    // Create Treatment Modal;
    'create_treatment_modal_button_save_text'   => 'Add New Treatment',
    'create_treatment_modal_button_save_icon'   => 'fa-save',
    'create_treatment_modal_button_cancel_text' => 'Cancel',
    'create_treatment_modal_button_cancel_icon' => 'fa-times',

    'modals' => [
        'delete_title'         => 'Delete',
        'delete_btn'           => 'Confirm Delete',
        'cancel_btn'           => 'Cancel',
        'delete_role_title'    => 'Delete Role',
        'delete_role_message'  => 'Are you sure you want to delete this role?',
        'delete_butik_title'   => 'Delete Butik',
        'delete_butik_message' => 'Are you sure you want to delete this Butik?',
    ],

];

<?php

return [
    //TITLE
    'userManagement' => "Users Management",
    'roleManagement' => "Roles Management",
    'createNewUser'  => "Create New User",
    'createNewRole'  => "Create New Role",
    'userEdit'       => "Edit",
    'roleEdit'       => "Edit Role",
    'userShow'       => "User Details",
    'roleShow'       => "Role Details",

    //USERS
    'username'       => "Username",
    'fullname'       => "Fullname",
    'email'          => "Email",
    'password'       => "Password",
    'confirm_pass'   => "Confirm Password",
    'roles'          => "Roles",

    //ROLES
    'roleName'       => 'Role Name',
    'permission'     => 'Permission',
    'noPermission'   => 'No Permission',

    //BUTTON
    'submit'         => "<i class='fas fa-paper-plane mr-1'></i> Submit",
    'update'         => "<i class='fas fa-paper-plane mr-1'></i> Update",
    'show'           => "<i class='fas fa-info-circle mr-1'></i>Show",
    'edit'           => "<i class='fas fa-user-edit mr-2'></i>Edit",
    'delete'         => "<i class='fas fa-trash-alt'></i>Delete",
    'back'           => "<i class='fas fa-arrow-circle-left mr-1'></i>Back",
    'newUser'        => "<i class='fas fa-user-plus mr-2'></i>Add New User",
    'newRole'        => "<i class='fas fa-shield-alt mr-2'></i>Add New Role",

    'buttons' => [
        'back'                   => '<i class="fas fa-arrow-circle-left mr-1"></i>Back',
        'create-new'             => 'New Patient',
        'delete'                 => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs hidden-sm">Delete</span><span class="hidden-xs hidden-sm hidden-md"></span>',
        'show'                   => '<i class="fa fa-eye fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Show</span><span class="hidden-xs hidden-sm hidden-md"></span>',
        'edit'                   => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Edit</span><span class="hidden-xs hidden-sm hidden-md"></span>',
        'treatment'              => '<i class="fa fa-medkit fa-fw" aria-hidden="true"></i> <span class="hidden-xs hidden-sm">Treatment</span><span class="hidden-xs hidden-sm hidden-md"></span>',
        'back-to-clinics'        => '<span class="hidden-sm hidden-xs">Back to </span><span class="hidden-xs">Butik@Sihat2U</span>',
        'back-to-clinic'         => 'Back  <span class="hidden-xs">to Butik@Sihat2U</span>',
        'delete-user'            => '<i class="fa fa-trash-o fa-fw" aria-hidden="true"></i>  <span class="hidden-xs">Delete</span><span class="hidden-xs"> Patient</span>',
        'edit-user'              => '<i class="fa fa-pencil fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Edit</span><span class="hidden-xs"> Patient</span>',
        'treatment-patient'      => '<i class="fa fa-stethoscope fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Treatment</span><span class="hidden-xs"></span>',
        'treatment-edit-patient' => '<i class="fa fa-stethoscope fa-fw" aria-hidden="true"></i> <span class="hidden-xs">Edit Treatment</span><span class="hidden-xs"></span>',
    ],
]
?>

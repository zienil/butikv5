<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\LocalizationController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\ButikController;
use App\Http\Controllers\PatientController;
use App\Http\Controllers\TreatmentController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('auth.login');
});
Route::get('lang/{lang}', [LocalizationController::class, 'switchLang'])->name('lang.switch');

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::group(['middleware' => ['auth']], function () {
    Route::resource('roles', RoleController::class);
    Route::resource('users', UserController::class);
    Route::resource('butiks', ButikController::class);

    //PATIENTS
    Route::resource('patients', PatientController::class);
    // Search function
    Route::post('patients/search', ['as' => 'patients.search', 'uses' => 'App\Http\Controllers\PatientController@search']);

    //AVATAR
    // Activate
    Route::get('/profile/activate', '\App\Http\Controllers\ProfileController@activateAvatar')->name('profile.activate');
    // Route to show user avatar
    Route::get('images/profile/{id}/avatar/{image}', ['uses' => 'App\Http\Controllers\ProfileController@userProfileAvatar',]);
    // Route to upload user avatar.
    Route::post('avatar/upload', ['as' => 'avatar.upload', 'uses' => 'App\Http\Controllers\ProfileController@upload']);

    //BUTIK IMAGE
    // Route to upload Butik Image.
    Route::post('butiks/upload/{id}', ['as' => 'butiks.upload', 'uses' => 'App\Http\Controllers\ButikController@upload']);

    //PROFILE
    //    Route::resource('profile', ProfileController::class, ['only' => ['index']]);
    Route::get('/profile/{username}', '\App\Http\Controllers\ProfileController@index')->name('profile.index');
    Route::get('/profile/{username}/edit', '\App\Http\Controllers\ProfileController@edit')->name('profile.edit');
    Route::get('/profile/{username}/create', '\App\Http\Controllers\ProfileController@createProfile')->name('profile.create');
    Route::match(['put', 'patch'], '/profile/{username}/update', '\App\Http\Controllers\ProfileController@update')->name('profile.update');
    Route::put('/profile/{username}/updateUserAccount', '\App\Http\Controllers\ProfileController@updateUserAccount')->name('profile.updateUserAccount');
    Route::put('/profile/{username}/updateUserPassword', '\App\Http\Controllers\ProfileController@updateUserPassword')->name('profile.updateUserPassword');

    //TREATMENT
    Route::resource('treatments', TreatmentController::class);
    Route::get('/treatments/patient/{patient}', 'App\Http\Controllers\TreatmentController@index')->name('treatment.patient');
    Route::get('/treatments/create/{treatment}', 'App\Http\Controllers\TreatmentController@create')->name('treatment.create');
});

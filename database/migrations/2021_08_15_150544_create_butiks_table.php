<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateButiksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('butiks', function (Blueprint $table) {
            $table->id();
            $table->string('butik_code')->nullable();
            $table->string('butik_name')->nullable();
            $table->string('butik_address')->nullable();
            $table->string('butik_email')->nullable();
            $table->string('butik_phone')->nullable();
            $table->string('butik_pic')->nullable();
            $table->string('butik_service')->nullable();
            $table->string('butik_image')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('butiks');
    }
}

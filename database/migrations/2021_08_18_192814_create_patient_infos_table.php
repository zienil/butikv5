<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_infos', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('patients_id')->unsigned()->index();
            $table->foreign('patients_id')->references('id')->on('patients')->onDelete('cascade');
            $table->string('gender')->nullable();
            $table->integer('age')->nullable();
            $table->string('religion')->nullable();
            $table->string('marital')->nullable();
            $table->integer('citizenship')->nullable()->comment('0: none citizen; 1: citizen');
            $table->string('nationality')->nullable();
            $table->string('race')->nullable();
            $table->string('email')->nullable();
            $table->string('occupation')->nullable();
            $table->string('address')->nullable();
            $table->string('postcode')->nullable();
            $table->string('state')->nullable();
            $table->string('city')->nullable();
            $table->string('image')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_infos');
    }
}

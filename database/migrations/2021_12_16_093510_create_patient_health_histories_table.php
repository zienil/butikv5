<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientHealthHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_health_histories', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('patient_treatments_id')->unsigned()->index();
            $table->foreign('patient_treatments_id')->references('id')->on('patient_treatments')->onDelete('cascade');
            $table->integer('diseaseType');
            $table->string('diseaseInfo')->nullable();
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_health_histories');
    }
}

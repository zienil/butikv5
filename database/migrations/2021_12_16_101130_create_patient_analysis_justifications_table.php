<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientAnalysisJustificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_analysis_justifications', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('patient_treatments_id')->unsigned()->index();
            $table->foreign('patient_treatments_id')->references('id')->on('patient_treatments')->onDelete('cascade');
            $table->string('das');
            $table->string('tls1');
            $table->string('tls2');
            $table->string('tls3');
            $table->string('ps');
            $table->string('noteAnalysis');
            $table->string('tkms');
            $table->string('pkm');
            $table->longText('justification');
            $table->longText('contraIndicator');
            $table->longText('proposalTherapeutic');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_analysis_justifications');
    }
}

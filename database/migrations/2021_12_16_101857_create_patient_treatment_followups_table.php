<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientTreatmentFollowupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_treatment_followups', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('patient_treatments_id')->unsigned()->index();
            $table->foreign('patient_treatments_id')->references('id')->on('patient_treatments')->onDelete('cascade');
            $table->longText('3days')->nullable()->comment('Next 3 days');
            $table->longText('7days')->nullable()->comment('Next 7 days');
            $table->longText('14days')->nullable()->comment('Next 14 days');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_treatment_followups');
    }
}

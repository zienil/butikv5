<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePatientTreatmentObservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_treatment_observations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('patient_treatments_id')->unsigned()->index();
            $table->foreign('patient_treatments_id')->references('id')->on('patient_treatments')->onDelete('cascade');
            $table->string('therapyType');
            $table->longText('observation')->nullable()->comment('Observation During Therapy');
            $table->longText('references')->nullable()->comment('References to Other Practitioners');
            $table->longText('followup')->nullable()->comment('Follow-up Therapy Plan');
            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_treatment_observations');
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Role::where('name', '=', 'System Admin')->first() === null) {
            Role::create([
                'name'       => 'System Admin',
                'guard_name' => 'web',
            ]);
        }
        if (Role::where('name', '=', 'System Support')->first() === null) {
            Role::create([
                'name'       => 'System Support',
                'guard_name' => 'web',
            ]);
        }
        if (Role::where('name', '=', 'Pendaftar')->first() === null) {
            Role::create([
                'name'       => 'Pendaftar',
                'guard_name' => 'web',
            ]);
        }
        if (Role::where('name', '=', 'Perunding')->first() === null) {
            Role::create([
                'name'       => 'Perunding',
                'guard_name' => 'web',
            ]);
        }
        if (Role::where('name', '=', 'Perunding Pelatih')->first() === null) {
            Role::create([
                'name'       => 'Perunding Pelatih',
                'guard_name' => 'web',
            ]);
        }
        if (Role::where('name', '=', 'Juruterapi')->first() === null) {
            Role::create([
                'name'       => 'Juruterapi',
                'guard_name' => 'web',
            ]);
        }
        if (Role::where('name', '=', 'Dispensari')->first() === null) {
            Role::create([
                'name'       => 'Dispensari',
                'guard_name' => 'web',
            ]);
        }
        if (Role::where('name', '=', 'User')->first() === null) {
            Role::create([
                'name'       => 'User',
                'guard_name' => 'web',
            ]);
        }
    }
}

<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
            'role-list',
            'role-create',
            'role-edit',
            'role-delete',
            'permission-list',
            'permission-create',
            'permission-edit',
            'permission-delete',
            'user-list',
            'user-create',
            'user-edit',
            'user-delete',
            'user-loginas',
            'butik-list',
            'butik-create',
            'butik-edit',
            'butik-delete',
            'patient-list',
            'patient-create',
            'patient-edit',
            'patient-delete',
            'treatment-list',
            'treatment-create',
            'treatment-edit',
            'treatment-delete',
        ];

        foreach ($permissions as $permission) {
            $result = Permission::where('name', $permission)->first();
            if (!$result) {
                Permission::create(['name' => $permission]);
            }
        }
    }
}

<?php

namespace Database\Seeders;

use App\Models\TreatmentType;
use App\Traits\GenerateUniqId;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TreatmentTypeSeeder extends Seeder
{
    use GenerateUniqId;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $createBy = 'system';
        $updateBy = 'system';

        $treatment = TreatmentType::create([
            'treatmentCode' => $this->getUniq(2),
            'descriptions'  => 'Modern Medicine / Perubatan Alopati',
            'created_by'    => $createBy,
            'updated_by'    => $updateBy,
        ]);
        $treatment->save();

        $treatment2 = TreatmentType::create([
            'treatmentCode' => $this->getUniq(2),
            'descriptions'  => 'Hemeopati Medical / Perubatan Homeopati',
            'created_by'    => $createBy,
            'updated_by'    => $updateBy,
        ]);
        $treatment2->save();

        $treatment2 = TreatmentType::create([
            'treatmentCode' => $this->getUniq(2),
            'descriptions'  => 'Complementary Medicine / Perubatan Komplementari',
            'created_by'    => $createBy,
            'updated_by'    => $updateBy,
        ]);
        $treatment2->save();

        $treatment4 = TreatmentType::create([
            'treatmentCode' => $this->getUniq(2),
            'descriptions'  => 'Islamic Medicine / Perubatan Islam',
            'created_by'    => $createBy,
            'updated_by'    => $updateBy,
        ]);
        $treatment4->save();

        $treatment5 = TreatmentType::create([
            'treatmentCode' => $this->getUniq(2),
            'descriptions'  => 'Traditional Medicine / Perubatan Tradisional',
            'created_by'    => $createBy,
            'updated_by'    => $updateBy,
        ]);
        $treatment5->save();
    }
}

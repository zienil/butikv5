<?php

namespace Database\Seeders;

use App\Models\DiseaseType;
use App\Traits\GenerateUniqId;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class DiseaseTypeSeeder extends Seeder
{
    use GenerateUniqId;

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $createBy = 'system';
        $updateBy = 'system';

        $disease = DiseaseType::create([
            'diseaseCode'  => $this->getUniq(2),
            'descriptions' => 'Kencing Manis',
            'created_by'   => $createBy,
            'updated_by'   => $updateBy,
        ]);
        $disease->save();

        $disease2 = DiseaseType::create([
            'diseaseCode'  => $this->getUniq(2),
            'descriptions' => 'Darah Tinggi',
            'created_by'   => $createBy,
            'updated_by'   => $updateBy,
        ]);
        $disease2->save();

        $disease2 = DiseaseType::create([
            'diseaseCode'  => $this->getUniq(2),
            'descriptions' => 'Masalah Jantung',
            'created_by'   => $createBy,
            'updated_by'   => $updateBy,
        ]);
        $disease2->save();

        $disease4 = DiseaseType::create([
            'diseaseCode'  => $this->getUniq(2),
            'descriptions' => 'Serangan Jantung',
            'created_by'   => $createBy,
            'updated_by'   => $updateBy,
        ]);
        $disease4->save();

        $disease5 = DiseaseType::create([
            'diseaseCode'  => $this->getUniq(2),
            'descriptions' => 'Kolesterol',
            'created_by'   => $createBy,
            'updated_by'   => $updateBy,
        ]);
        $disease5->save();

        $disease6 = DiseaseType::create([
            'diseaseCode'  => $this->getUniq(2),
            'descriptions' => 'Kanser / Ketumbuhan',
            'created_by'   => $createBy,
            'updated_by'   => $updateBy,
        ]);
        $disease6->save();

        $disease7 = DiseaseType::create([
            'diseaseCode'  => $this->getUniq(2),
            'descriptions' => 'Urat Simpul / Vena Verikos (Varicose Vein)',
            'created_by'   => $createBy,
            'updated_by'   => $updateBy,
        ]);
        $disease7->save();

        $disease8 = DiseaseType::create([
            'diseaseCode'  => $this->getUniq(2),
            'descriptions' => 'Trombosis Vena (Deep Vein Thrombosis)',
            'created_by'   => $createBy,
            'updated_by'   => $updateBy,
        ]);
        $disease8->save();

        $disease9 = DiseaseType::create([
            'diseaseCode'  => $this->getUniq(2),
            'descriptions' => 'Patah Tulang',
            'created_by'   => $createBy,
            'updated_by'   => $updateBy,
        ]);
        $disease9->save();

        $disease10 = DiseaseType::create([
            'diseaseCode'  => $this->getUniq(2),
            'descriptions' => 'Pembedahan',
            'created_by'   => $createBy,
            'updated_by'   => $updateBy,
        ]);
        $disease10->save();

        $disease11 = DiseaseType::create([
            'diseaseCode'  => $this->getUniq(2),
            'descriptions' => 'Bersalin',
            'created_by'   => $createBy,
            'updated_by'   => $updateBy,
        ]);
        $disease11->save();

        $disease12 = DiseaseType::create([
            'diseaseCode'  => $this->getUniq(2),
            'descriptions' => 'Lain-Lain',
            'created_by'   => $createBy,
            'updated_by'   => $updateBy,
        ]);
        $disease12->save();
    }
}

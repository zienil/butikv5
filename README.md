## About Butikv4@Sihat2U

---

## NOTE

-------------------------------------------------------------------------

## Installation Instructions

1. Run clone
    - `git clone `
2. Run `npm install`
3. Run `composer install` (To create Vendor folder)
4. Create a MySQL database for the project
    - `mysql -u root -p`
    - `create database butik;`
    - `exit`
5. From the projects root run :
    - `cp .env.example .env`
6. Configure your `.env` file
7. From the projects root folder run :
    - `php artisan key:generate`
    - `php artisan migrate`
    - `composer dump-autoload`
    - `php artisan db:seed`
    - `php artisan passport:install`

-------------------------------------------------------------------------

### Rolling Back Migrations

1. Rollback The Last Migration Operation :
    - `php artisan migrate:rollback`
2. Rollback all migrations :
    - `php artisan migrate:reset`
3. Rollback all migrations and run them all again :
    - `php artisan migrate:refresh`
    - `php artisan migrate:refresh --seed`

-------------------------------------------------------------------------

### Laravel Artisan Command

1. Check Laravel Version :
    - `php artisan --version`
2. Re-optimized class loader :
    - `php artisan optimize:clear`
3. Clear Cache facade value :
    - `php artisan cache:clear`
4. Clear Route cache :
    - `php artisan route:clear`
5. Clear View cache :
    - `php artisan view:clear`
6. Clear Config cache :
    - `php artisan config:clear`
7. Build Cache :
    - `php artisan config:cache`
8. After delete controller :
    - `composer dump-autoload -o`
9. Create Controller with resource and model :
    - `php artisan make:controller TestController -r -m`
10. Make model with migration :
    - `php artisan make:model Model_Name -m`
11. Maintenance mode on : `php artisan down`
12. Maintenance mode off : `php artisan up`
13. To create the symbolic link between storage folder and public folder : `php artisan storage:link`

#### Special Laravel Artisan Command

1. Run Certain Seeder (To update Permission List) : `php artisan db:seed --class=PermissionTableSeeder`
2. Laravel run specific migration :
    - `php artisan migrate --path=/database/migrations/my_migration.php`
    - `php artisan migrate:refresh --path=/database/migrations/fileName.php`
    - `php artisan migrate:rollback --step=1/2/3`

-----------------------------------------------------------------------------

### Build the Front End Assets with Mix

#### Using NPM :

1. Provides live reloading. This should be used for development only : `npm run dev`
2. Run scripts from package.json when files change : `npm run watch`
3. If `npm run watch` don't work :
    - remove node_modules :
        - `rm -rf node_modules && npm install`

#### Using Yarn:

1. From the projects root folder run  : `yarn install`
2. From the projects root folder run :
    - `yarn run dev` or
    - `yarn run production`

* You can watch assets with : `yarn run watch`

---------------------------------------------------------------------------

### Plugin Installation

1. Bootstrap 4.6 : `npm i bootstrap@4.6.0`
2. Font Awesome Free : `npm install @fortawesome/fontawesome-free`
3. CoreUI : `npm i @coreui/coreui@3.4.0`
4. CoreUI Icons : `npm install @coreui/icons --save`
5. perfect-scrollbar : `npm install perfect-scrollbar`
6. pace-progress : `npm i pace-progress --save`
7. Install Simple Line Icons :`npm install simple-line-icons --save`
8. PopperJS@core : `npm i @popperjs/core`
9. Hover.css : `npm install hover.css --save`
10. Flag Icon Css : `npm i flag-icon-css`
11. Jquery HideshowPassword : `npm install --save hideshowpassword`
12. JQuery Password-Strength-Meter : `npm install --save password-strength-meter`
13. Spatie : `composer require spatie/laravel-permission`
14. Spatie Laravelcollective-html : `composer require laravelcollective/html`
15. Dropzone : `npm i dropzone`

------------------------------------------------------------------------

### Seeded Users

|Email|Username|Password|Access|
|:------------|:------------|:------------|:------------|
|admin@gmail.com|admin|123456|System Admin Access|

------------------------------------------------------------------------

### InvalidArgumentException  :

    Please provide a valid cache path.

Need to create bellow folder inside storage folder

    -   framework
            -   cache
            -   sessions
            -   views

-------------------------------------------------------------------------

### .ENV FILE

Copy and paste bellow code inside .env file

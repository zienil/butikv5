<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUserPasswordRequest;
use App\Http\Requests\UpdateUserProfile;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use File;
use Image;
use Validator;

class ProfileController extends Controller
{

    public function index($username)
    {
        //        $id   = Auth::user()->id;
        //        $user = User::find($id);
        $user = $this->getUserByUsername($username);
        //        dd($user);
        return view('profiles.index', compact('user'));
    }

    public function edit($username)
    {
        try {
            $user = $this->getUserByUsername($username);
            //            dd($user);
        } catch (ModelNotFoundException $exception) {
            return view('pages.status')
                ->with('error', trans('profile.notYourProfile'))
                ->with('error_title', trans('profile.notYourProfileTitle'));
        }

        if ($user->profile == null) {
            $profile = new Profile();
            $user->profile()->save($profile);
        }

        return view('profiles.edit', compact('user'));
    }

    public function updateUserAccount(Request $request, $id)
    {
        $id         = Auth::user()->id;
        $user       = User::find($id);
        $emailCheck = ($request->input('email') != '') && ($request->input('email') != $user->email);
        $rules      = [];

        if ($user->username != $request->input('username')) {
            $usernameRules = [
                'username' => 'required|max:255|unique:users,username',
            ];
        } else {
            $usernameRules = [
                'username' => 'required|max:255',
            ];
        }
        if ($emailCheck) {
            $emailRules = [
                'email' => 'email|max:255|unique:users,email',
            ];
        } else {
            $emailRules = [
                'email' => 'email|max:255',
            ];
        }

        $rules     = array_merge($usernameRules, $emailRules);
        $validator = Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $user->username = $request->input('username');
        $user->fullname = $request->input('fullname');
        if ($emailCheck) {
            $user->email = $request->input('email');
        }
        $user->save();

        return redirect('profile/'.$user->username.'/edit')->with('success', trans('profile.updateAccountSuccess'));
    }

    public function updateUserPassword(UpdateUserPasswordRequest $request, $id)
    {
        $user  = User::findOrFail($id);
        $input = $request->all();

        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }
        $user->update($input);

        return redirect('profile/'.$user->username.'/edit')->with('success', trans('profile.updatePWSuccess'));
    }

    public function update(UpdateUserProfile $request, $username)
    {
        $user = $this->getUserByUsername($username);

        $input = $request->only('icnumber', 'phone', 'age', 'gender', 'race', 'occupation', 'address', 'postcode', 'state', 'country', 'avatar_status');

        //UPDATE PROFILE
        if ($user->profile == null) {
            $profile = new Profile();
            $profile->fill($input);
            $user->profile()->save($profile);
        } else {
            $user->profile->fill($input)->save();
        }

        return redirect('profile/'.$user->username.'/edit')->with('success', trans('profile.updateSuccess'));
    }

    public function createProfile($username)
    {
        try {
            $user = $this->getUserByUsername($username);
        } catch (ModelNotFoundException $exception) {
            return view('pages.status')
                ->with('error', trans('profile.notYourProfile'))
                ->with('error_title', trans('profile.notYourProfileTitle'));
        }

        if ($user->profile == null) {
            $profile = new Profile();
            $user->profile()->save($profile);
        }

        return view('profiles.index', compact('user'));
    }

    public function upload(Request $request)
    {
        if ($request->hasFile('file')) {
            $currentUser = Auth::user();

            //CHECK IF AVATAR EXIST OR NOT
            $filter = Profile::where('user_id', '=', $currentUser->id)->get();
            $path   = $filter[0]['avatar'];
            $url    = public_path().$path;
            //            dd($filter[0]['avatar']);

            if (empty($path)) {
                //                dd('empty');
                $this->avatar($request->file('file'), $currentUser->id);
            } else {
                //                dd('not empty');
                if (File::exists($url)) {
                    //                dd('File is Exists');
                    //DELETE OLD IMAGE
                    unlink($url);
                    $this->avatar($request->file('file'), $currentUser->id);
                } else {
                    //                dd('File is Not Exists');
                    $this->avatar($request->file('file'), $currentUser->id);
                }
            }
        } else {
            return response()->json(false, 200);
        }
    }

    /**
     * Show user avatar.
     *
     * @param $id
     * @param $image
     *
     * @return string
     */
    public function userProfileAvatar($id, $image)
    {
        return Image::make(storage_path('app/public').'/profile/avatar/'.$id.'/'.$image)->response();
    }

    public function avatar($file, $userid)
    {
        $currentUser = Auth::user();
        $avatar      = $file;
        $filename    = 'avatar.'.$avatar->getClientOriginalExtension();
        $save_path   = storage_path('app/public').'/profile/avatar/'.$userid.'/';
        $path        = $save_path.$filename;
        $public_path = '/storage/profile/avatar/'.$userid.'/'.$filename;

        // Make the user a folder and set permissions
        File::makeDirectory($save_path, $mode = 0755, true, true);

        // Save the file to the server
        Image::make($avatar)->resize(300, 300)->save($save_path.$filename);

        // Save the public image path
        $currentUser->profile->avatar        = $public_path;
        $currentUser->profile->avatar_status = 1;
        $currentUser->profile->save();

        return response()->json(['path' => $path], 200);
    }

    public function activateAvatar()
    {
        $id                     = Auth::user()->id;
        $profile                = new Profile();
        $profile->user_id       = $id;
        $profile->avatar        = '';
        $profile->avatar_status = 1;
        $profile->save();

        return redirect()->route('profile.edit')->with('success', 'Avatar activate successfully');
    }

    public function getUserByUsername($username)
    {
        return User::with('profile')->whereusername($username)->firstOrFail();
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\PatientDiagnosis;
use App\Models\Patients;
use App\Models\PatientTreatment;
use App\Models\DiseaseType;
use App\Models\TreatmentType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TreatmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request, Patients $patient)
    {
        $treatment = $patient->treatment;
        //        dd($treatment);
        return view('treatments.index', compact('patient', 'treatment'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($treatment)
    {
        //        dd($treatment);
        //        $patientid     = $treatment->patients_id;
        $patient       = PatientTreatment::where('id', $treatment)->first();
        $diseaseType   = DiseaseType::all('id', 'diseaseCode', 'descriptions');
        $treatmentType = TreatmentType::all('id', 'treatmentCode', 'descriptions');
        //        $patientTreatmentType = PatientTreatment::where('id', $treatment)->get();
        //
        //        foreach ($patientTreatmentType as $rowTreatment) {
        //            $patientid          = $rowTreatment->patient_id;
        //            $ptreatmentid       = $rowTreatment->id;
        //            $ptreatmentType     = $rowTreatment->treatmentType;
        //            $ptreatmentTypeDesc = $rowTreatment->treatmentTypeDesc;
        //        }
        //        $patientDiagnoses = PatientDiagnosis::select('currentDiagnoses')->where('patient_treatments_id', $treatment)->get();
        //
        //        return view('treatments.create', compact('diseaseType', 'treatmentType', 'patientid', 'ptreatmentid', 'ptreatmentType', 'ptreatmentTypeDesc', 'patientDiagnoses'));
        return view('treatments.create', compact('patient', 'diseaseType', 'treatmentType'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $action = $request->input('action');

        //1. Treatment Type
        if ($action === 'Treatment') {
            $patientID = $request->input('patientID');
            $ptt       = PatientTreatment::create([
                'patients_id'       => $patientID,
                'treatmentType'     => $request->input('treatment_type'),
                'treatmentTypeDesc' => $request->input('others'),
                'created_by'        => Auth::user()->username,
            ]);
            $ptt->save();
            return redirect('/treatments/patient/'.$patientID)->with('success', trans('treatment.createSuccess'));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Butiks;
use App\Models\ButikUser;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Validator;
use File;
use Image;

//use Intervention\Image\Image;

class ButikController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:butik-list|butik-create|butik-edit|butik-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:butik-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:butik-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:butik-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $butik = Butiks::orderBy('id', 'ASC')->get();
        return view('butiks.index', compact('butik'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('butiks.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'clinicName'    => 'required|max:100',
                'clinicAddress' => 'required|max:255',
                'clinicEmail'   => 'required|email|max:255',
                'clinicPhone'   => 'required|max:10',
                'clinicPIC'     => 'required|max:100',
                'clinicService' => 'required',
            ],
            [
                'clinicName.required'      => trans('auth.clinicNameRequired'),
                'clinicAddress.required'   => trans('auth.clinicAddRequired'),
                'clinicEmail.required'     => trans('auth.clinicEmailRequired'),
                'clinicEmail.clinic_email' => trans('auth.clinicEmailInvalid'),
                'clinicPhone.required'     => trans('auth.clinicPhoneRequired'),
                'clinicPIC.required'       => trans('auth.clinicPICRequired'),
                'clinicService.required'   => trans('auth.clinicServiceRequired'),
            ]
        );

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        $butik = Butiks::create([
            'butik_code'    => Str::random(4),
            'butik_name'    => $request->input('clinicName'),
            'butik_address' => $request->input('clinicAddress'),
            'butik_email'   => $request->input('clinicEmail'),
            'butik_phone'   => $request->input('clinicPhone'),
            'butik_pic'     => $request->input('clinicPIC'),
            'butik_service' => $request->input('clinicService'),
            'created_by'    => Auth::user()->username,
        ]);
        $butik->save();

        return view('butiks.index')->with('success', trans('clinic.createSuccess'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Butiks $butik)
    {
        $userButik = ButikUser::where('butik_id', '=', $butik->id)->get();
        $users     = DB::table('users')
                       ->join('profiles', 'users.id', '=', 'profiles.user_id')
                       ->join('butik_users', 'users.id', '=', 'butik_users.user_id')
                       ->join('butiks', 'butik_users.butik_id', '=', 'butiks.id')
                       ->join('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
                       ->join('roles', 'model_has_roles.role_id', '=', 'roles.id')
                       ->select('users.fullname', 'users.email', 'profiles.phone', 'roles.id AS roleid')
                       ->where('butiks.id', '=', $butik->id)
                       ->whereIn('roles.id', [4, 6])
                       ->get();
//        dd($users);
        return view('butiks.show', compact('butik', 'userButik','users'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Butiks $butik)
    {
        return view('butiks.edit', compact('butik'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Butiks::find($id)->delete();
        return redirect()->route('butiks.index')->with('success', trans('clinic.deleteSuccess'));
    }

    public function upload(Request $request, Butiks $butik)
    {
        if ($request->hasFile('file')) {
            $butikID = $request->id;

            //CHECK IF AVATAR EXIST OR NOT
            $filter = Butiks::where('id', '=', $butikID)->get();
            $path   = $filter[0]['butik_image'];
            $url    = public_path().$path;
            //                        dd($filter[0]['avatar']);

            if (empty($path)) {
                //                dd('empty');
                $this->avatar($request->file('file'), $butikID);
            } else {
                //                dd('not empty');
                if (File::exists($url)) {
                    //                dd('File is Exists');
                    //DELETE OLD IMAGE
                    unlink($url);
                    $this->avatar($request->file('file'), $butikID);
                } else {
                    //                dd('File is Not Exists');
                    $this->avatar($request->file('file'), $butikID);
                }
            }
        } else {
            return response()->json(false, 200);
        }
    }

    public function avatar($file, $butikID)
    {
        $currentButik = Butiks::find($butikID);
        $avatar       = $file;
        $filename     = 'avatar.'.$avatar->getClientOriginalExtension();
        $save_path    = storage_path('app/public').'/butik/avatar/'.$butikID.'/';
        $path         = $save_path.$filename;
        $public_path  = '/storage/butik/avatar/'.$butikID.'/'.$filename;

        // Make the user a folder and set permissions
        File::makeDirectory($save_path, $mode = 0755, true, true);

        // Save the file to the server
        Image::make($avatar)->resize(500, 300)->save($save_path.$filename);

        // Save the public image path
        $currentButik->butik_image = $public_path;
        $currentButik->updated_by  = Auth::user()->username;
        $currentButik->save();

        return response()->json(['path' => $path], 200);
    }
}

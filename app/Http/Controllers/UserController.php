<?php

namespace App\Http\Controllers;

use App\Models\Butiks;
use App\Models\ButikUser;
use App\Models\Profile;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Gate;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Spatie\Permission\Models\Role;

class UserController extends Controller
{

    function __construct()
    {
        $this->middleware('permission:user-list|user-create|user-edit|user-delete|user-loginas', ['only' => ['index', 'store']]);
        $this->middleware('permission:user-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:user-edit', ['only' => ['edit', 'update']]);
        //        $this->middleware('permission:user-loginas', ['only' => ['impersonate']]);
        $this->middleware('permission:user-delete', ['only' => ['destroy']]);
    }

    public function index(Request $request)
    {
        $data = User::orderBy('created_at', 'DESC')->get();
        return view('users.index', compact('data'));
    }

    public function create()
    {
        $roles = Role::pluck('name', 'name')->all();
        $butik = Butiks::pluck('butik_name', 'id')->all();
        //        dd($roles);
        return view('users.create', compact('roles', 'butik'));
    }

    public function store(Request $request)
    {
        $profile = new Profile();

        $this->validate($request, [
            'username' => 'required|unique:users,username',  //check unique on table users, column username
            'fullname' => 'required',
            'email'    => 'required|email|unique:users,email', //check unique on table users, column email
            'password' => 'required|same:confirm-password',
            'roles'    => 'required'
        ]);
        $input = $request->all();

        $input['password'] = Hash::make($input['password']);
        $user              = User::create($input);


        $user->butik()->create([
            'butik_id' => $request->input('butik'),
        ]);

        $user->assignRole($request->input('roles'));
        $user->profile()->save($profile);

        return redirect()->route('users.index')->with('success', 'User created successfully');
    }

    public function show($id)
    {
        $user      = User::find($id);
        $butik     = Butiks::all();
        $userButik = ButikUser::where('user_id', '=', $id)->get();
        return view('users.show', compact('user', 'butik', 'userButik'));
    }

    public function edit($id)
    {
        $user  = User::find($id);
        $roles = Role::pluck('name', 'name')->all();
        //        $butik = Butiks::pluck('butik_name', 'id')->all();
        //        $userButik = $user->butik->pluck('id','butik_id')->all();
        $butik     = Butiks::all();
        $userButik = ButikUser::where('user_id', '=', $id)->get();
        //                dd($userButik);
        $userRole = $user->roles->pluck('name', 'name')->all();

        return view('users.edit', compact('user', 'roles', 'userRole', 'butik', 'userButik'));
    }

    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'fullname' => 'required',
            'email'    => 'required|email|unique:users,email,'.$id,//check unique on table users, column email
            'password' => 'same:confirm-password',
            'roles'    => 'required'
        ]);

        $input = $request->all();

        if (!empty($input['password'])) {
            $input['password'] = Hash::make($input['password']);
        } else {
            $input = Arr::except($input, array('password'));
        }

        $user = User::find($id);
        $user->update($input);

        DB::table('model_has_roles')->where('model_id', $id)->delete();
        $user->assignRole($request->input('roles'));

        $userButik = $request->input('butik');
        if ($userButik != null) {
            $user->butik()->update(['butik_id' => $request->input('butik')]);
        }
        return redirect()->route('users.index')->with('success', 'User updated successfully');
    }

    public function destroy($id)
    {
        User::find($id)->delete();
        return redirect()->route('users.index')->with('success', 'User deleted successfully');
    }

    public function impersonate($id)
    {
        $user = User::find($id);

        // Guard against administrator impersonate
        if (Gate::allows('user-loginas') && !$user->hasPermissionTo('user-create')) {
            Auth::user()->setImpersonating($user->id);
        } else {
            Session::flash('error', 'Impersonate disabled for this user.');
        }

        //return redirect()->back();
        return redirect()->route('home');
    }

    public function stopImpersonate()
    {
        Auth::user()->stopImpersonating();
        Session::flash('success', 'Welcome back!');
        //return redirect()->back();
        return redirect()->route('home');
    }
}

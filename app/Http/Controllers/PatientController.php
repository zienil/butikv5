<?php

namespace App\Http\Controllers;

use App\Models\Country;
use App\Models\Patients;
use App\Models\PatientInfo;
use App\Models\State;
use App\Traits\ExtractIcNumber;
use App\Traits\GenerateMrn;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;

class PatientController extends Controller
{
    use ExtractIcNumber;
    use GenerateMrn;

    function __construct()
    {
        $this->middleware('permission:patient-list|patient-create|patient-edit|patient-delete', ['only' => ['index', 'store']]);
        $this->middleware('permission:patient-create', ['only' => ['create', 'store']]);
        $this->middleware('permission:patient-edit', ['only' => ['edit', 'update']]);
        $this->middleware('permission:patient-delete', ['only' => ['destroy']]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patients = Patients::all();
        return view('patients.search-patients', compact('patients'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $countries = Country::all();
        $state     = State::where('country_id', '=', 1131)->orderBy('name')->get();
        return view('patients.create-patient', compact('countries', 'state'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //        $icRegx = '/^[0-9]{6}-[0-9]{2}-[0-9]{4}$/';
        //        if(preg_match($icRegx,$request->input('patientIc'))){
        //
        //        }else{
        //
        //        }
        $validator = Validator::make($request->all(),
            [
                'patientName'        => 'required|max:255',
                'patientIc'          => 'required|regex:/^[0-9]{6}-[0-9]{2}-[0-9]{4}$/',
                'patientPhone'       => 'required|max:12',
                'patientCitizen'     => 'required',
                'patientNationality' => 'required',
                'patientAddress'     => 'required',
                'patientPostcode'    => 'required|max:5',
                'patientCity'        => 'required',
                'patientState'       => 'required',
            ],
            [
                'patientName.required'        => trans('auth.patientNameRequired'),
                'patientIc.required'          => trans('auth.patientIcRequired'),
                'patientCitizen.required'     => trans('auth.patientCitizenRequired'),
                'patientNationality.required' => trans('auth.patientNationalityRequired'),
                'patientAddress.required'     => trans('auth.patientAddressRequired'),
                'patientPostcode.required'    => trans('auth.patientPostcodeRequired'),
                'patientCity.required'        => trans('auth.patientCityRequired'),
                'patientState.required'       => trans('auth.patientStateRequired'),
                'patientPhone.required'       => trans('auth.patientPhoneRequired'),
            ]
        );

        if ($validator->fails()) {
            return back()->withErrors($validator)->withInput();
        }

        // GET IC NUMBER DETAILS(DOB,GENDER,STATE);
        $icDetail = $this->get($request->input('patientIc'));
        $dob      = $icDetail['dob'];
        $state    = $icDetail['state'];
        $gender   = $icDetail['gender'];
        $age      = $icDetail['age'];

        $patient = Patients::create([
            'patientMRN'   => $this->getMRN(7),
            'full_name'    => $request->input('patientName'),
            'ic_number'    => $request->input('patientIc'),
            'phone_number' => $request->input('patientPhone'),
            'created_by'   => Auth::user()->username,
        ]);
        $patient->save();

        $patientInfo = PatientInfo::create([
            'patients_id' => $patient->id,
            'gender'      => $gender,
            'age'         => $age,
            'citizenship' => $request->input('patientCitizen'),
            'nationality' => $request->input('patientNationality'),
            'address'     => $request->input('patientAddress'),
            'postcode'    => $request->input('patientPostcode'),
            'city'        => $request->input('patientCity'),
            'state'       => $request->input('patientState'),
            'created_by'  => Auth::user()->username,
        ]);
        $patientInfo->save();

        return redirect('patients')->with('success', trans('patient.createSuccess'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patients     = Patients::find($id);
        $patientInfos = $patients->info;
        //                        dd($patientInfos);
        return view('patients.show-patient', compact('patients', 'patientInfos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patients = Patients::find($id);
        $patients->delete();
        return redirect('patients')->with('success', trans('patient.deleteSuccess'));
    }

    public function search(Request $request)
    {
        $searchIC     = $request->input('searchIC');
        $searchresult = Patients::where('ic_number', 'LIKE', '%'.$searchIC.'%')->get();
        $countSearch  = $searchresult->count();
        return view('patients.search-patients', compact('searchresult', 'searchIC', 'countSearch'));
    }
}

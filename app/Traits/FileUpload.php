<?php

namespace App\Traits;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

trait FileUpload
{
    public function annFileUpload($query, $category)
    {
        // Get filename with the extension
        $filenameWithExt = $query->getClientOriginalName();
        //Get just filename
        $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
        // Get just ext
        $extension = strtolower($query->getClientOriginalExtension());
        // Filename to store
        $fileNameToStore = $filename.'_'.time().'.'.$extension;
        // Upload Image
        Storage::disk('public')->put('announcement/'.$category.'/'.$fileNameToStore, file_get_contents($query));

        return $fileNameToStore; // Just return image
    }
}

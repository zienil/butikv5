<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Butiks extends Model
{
    use HasFactory;

    protected $table = 'butiks';
    protected $guarded = ['id'];

    protected $fillable = [
        'butik_code',
        'butik_name',
        'butik_address',
        'butik_email',
        'butik_phone',
        'butik_pic',
        'butik_service',
        'butik_image',
        'created_by',
        'updated_by',
    ];


}

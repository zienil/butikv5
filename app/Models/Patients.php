<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Patients extends Model
{
    use HasFactory;

    protected $table = 'patients';
    protected $primaryKey = 'id';

    protected $guarded = ['id'];

    protected $fillable = [
        'patientMRN',
        'full_name',
        'ic_number',
        'phone_number',
        'created_by',
        'updated_by',
    ];

    public function info()
    {
        return $this->hasOne(PatientInfo::class);
    }

    public function treatment()
    {
        return $this->hasMany(PatientTreatment::class)->orderBy('created_at', 'desc');
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientTreatmentObservation extends Model
{
    use HasFactory;

    protected $table = 'patient_treatment_observations';
    protected $guarded = 'id';
    protected $fillable = ['treatmentID', 'therapyType', 'observation', 'references', 'followup', 'created_by', 'updated_by'];

    public function treatment()
    {
        $this->belongsTo(PatientTreatment::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientTreatmentHistory extends Model
{
    use HasFactory;

    protected $table = 'patient_treatment_histories';
    protected $guarded = 'id';
    protected $fillable = ['treatmentID', 'treatmentType', 'treatmentInfo', 'created_by', 'updated_by'];

    public function treatment()
    {
        $this->belongsTo(PatientTreatment::class);
    }
}

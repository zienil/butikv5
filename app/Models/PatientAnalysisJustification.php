<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientAnalysisJustification extends Model
{
    use HasFactory;

    protected $table = 'patient_analysis_justifications';
    protected $guarded = 'id';
    protected $fillable = ['treatmentID', 'das', 'tls1', 'tls2', 'tls3', 'ps', 'noteAnalysis', 'tkms', 'pkm', 'justification', 'contraIndicator', 'proposalTherapeutic', 'created_by', 'updated_by'];

    public function treatment()
    {
        $this->belongsTo(PatientTreatment::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientTreatment extends Model
{
    use HasFactory;

    protected $table = 'patient_treatments';
    protected $guarded = 'id';
    protected $fillable = ['patients_id', 'treatmentType', 'treatmentTypeDesc', 'created_by'];

    public function patient()
    {
        return $this->belongsTo(Patients::class);
    }

    public function diagnosis()
    {
        return $this->hasOne(PatientDiagnosis::class);
    }

    public function history()
    {
        return $this->hasOne(PatientHealthHistory::class);
    }

    public function treathistory()
    {
        return $this->hasOne(PatientTreatmentHistory::class);
    }

    public function vital()
    {
        return $this->hasOne(PatientVitalSign::class);
    }

    public function analysis()
    {
        return $this->hasOne(PatientAnalysisJustification::class);
    }

    public function obsrvation()
    {
        return $this->hasOne(PatientTreatmentObservation::class);
    }

    public function followuop()
    {
        return $this->hasOne(PatientTreatmentFollowup::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TreatmentType extends Model
{
    use HasFactory;

    protected $table = 'treatment_types';
    protected $guarded = ['id'];
    protected $fillable = [
        'treatmentCode',
        'descriptions',
        'created_by',
        'updated_by',
    ];
}

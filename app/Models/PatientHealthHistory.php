<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientHealthHistory extends Model
{
    use HasFactory;

    protected $table = 'patient_health_histories';
    protected $guarded = 'id';
    protected $fillable = ['treatmentID', 'diseaseType', 'diseaseInfo', 'created_by', 'updated_by'];

    public function treatment()
    {
        $this->belongsTo(PatientTreatment::class);
    }
}

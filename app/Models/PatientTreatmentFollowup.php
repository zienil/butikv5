<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientTreatmentFollowup extends Model
{
    use HasFactory;

    protected $table = 'patient_treatment_followups';
    protected $guarded = 'id';
    protected $fillable = ['treatmentID', '3days', '7days', '14days', 'created_by', 'updated_by'];

    public function treatment()
    {
        $this->belongsTo(PatientTreatment::class);
    }
}

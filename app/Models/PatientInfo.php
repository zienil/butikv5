<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientInfo extends Model
{
    use HasFactory;

    protected $table = 'patient_infos';
    protected $primaryKey = 'id';

    protected $guarded = ['id'];

    protected $fillable = [
        'patients_id',
        'email',
        'age',
        'gender',
        'race',
        'religion',
        'martial',
        'citizenship',
        'nationality',
        'occupation',
        'address',
        'city',
        'postcode',
        'state',
        'image',
        'created_by',
        'updated_by',
    ];

    public function patient()
    {
        return $this->belongsTo(Patients::class);
    }
}

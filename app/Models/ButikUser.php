<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ButikUser extends Model
{
    use HasFactory;
    protected $table = 'butik_users';

    protected $guarded = [];

    protected $fillable = [
        'user_id',
        'butik_id',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

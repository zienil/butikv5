<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PatientVitalSign extends Model
{
    use HasFactory;

    protected $table = 'patient_vital_signs';
    protected $guarded = 'id';
    protected $fillable = ['treatmentID', 'bloodPresure', 'weight', 'height', 'bodyTemp', 'pulse', 'glucose', 'scalePain', 'created_by', 'updated_by'];

    public function treatment()
    {
        $this->belongsTo(PatientTreatment::class);
    }
}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Profile extends Model
{
    use HasFactory;

    protected $table = 'profiles';
    protected $guarded = [
        'id',
    ];

    protected $fillable = [
        'avatar',
        'avatar_status',
        'icnumber',
        'phone',
        'age',
        'gender',
        'race',
        'occupation',
        'address',
        'postcode',
        'state',
        'country'
    ];

    /**
     * A profile belongs to a user.
     *
     * @return mixed
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}

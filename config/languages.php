<?php

return [
    'en' => [
        'display'   => 'English',
        'flag-icon' => 'us'
    ],
    'my' => [
        'display'   => 'Malay',
        'flag-icon' => 'my'
    ],
];
